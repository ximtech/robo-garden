#include "DHTModule.h"
#include <avr/interrupt.h>

#define TIMER2_VALUE_FOR_FOUR_SECONDS 31250

static volatile ReadStatus readStatus;

static volatile ZoneDHTData zone1DhtData = { 
	.sensorStatus = DHT_SENSOR_NOT_CONNECTED, 
	.integralRH = 0, 
	.decimalRH = 0, 
	.integralT = 0, 
	.decimalT = 0, 
	.isMaxTempEventOccured = false, 
	.isMinTempEventOccured = false 
};
	
static volatile ZoneDHTData zone2DhtData = { 
	.sensorStatus = DHT_SENSOR_NOT_CONNECTED, 
	.integralRH = 0, 
	.decimalRH = 0, 
	.integralT = 0, 
	.decimalT = 0, 
	.isMaxTempEventOccured = false, 
	.isMinTempEventOccured = false 
};

static volatile ZoneDHTData zone3DhtData = { 
	.sensorStatus = DHT_SENSOR_NOT_CONNECTED, 
	.integralRH = 0, 
	.decimalRH = 0, 
	.integralT = 0, 
	.decimalT = 0, 
	.isMaxTempEventOccured = false, 
	.isMinTempEventOccured = false 
};

static volatile ZoneDHTData zone4DhtData = { 
	.sensorStatus = DHT_SENSOR_NOT_CONNECTED, 
	.integralRH = 0, 
	.decimalRH = 0, 
	.integralT = 0, 
	.decimalT = 0, 
	.isMaxTempEventOccured = false, 
	.isMinTempEventOccured = false 
};

static void processZoneDHTData();
static void mapZoneDhtData(volatile ZoneDHTData *zoneDhtData, volatile ReadStatus readStatus);
static DHTSensorStatus getSensorStatus(ReadStatus readStatus);
static void checkMaxTempEvent(ZoneSettings *zoneSettings, volatile ZoneDHTData *zoneDhtData);
static void checkMinTempEvent(ZoneSettings *zoneSettings, volatile ZoneDHTData *zoneDhtData);
static _Bool isTempNowIsHigherOrEqualsThanMax(Temperature *temperature, volatile ZoneDHTData *zoneDhtData);
static _Bool isTempNowLessOrEqualsThanHysteresisMax(Temperature *temperature, volatile ZoneDHTData *zoneDhtData);
static _Bool isTempNowIsLessOrEqualsThanMin(Temperature *temperature, volatile ZoneDHTData *zoneDhtData);
static _Bool isTempNowHigherOrEqualsThanHysteresisMin(Temperature *temperature, volatile ZoneDHTData *zoneDhtData);
static void switchOnMaxTempOutput(ZoneNumber zoneNumber);
static void switchOffMaxTempOutput(ZoneNumber zoneNumber);
static void switchOnMinTempOutput(ZoneNumber zoneNumber);
static void switchOffMinTempOutput(ZoneNumber zoneNumber);


ISR(TIMER3_OVF_vect) {
	processZoneDHTData();
	TCNT3 = TIMER2_VALUE_FOR_FOUR_SECONDS;
}


void initDHTHandler() {
	
	ZONE_TEMPERATURE_OUTPUT_DDR = ZONE_TEMPERATURE_OUTPUT_DDR	// configure as output for zones
	| (1 << ZONE_1_MAX_TEMPERATURE_DDR)
	| (1 << ZONE_2_MAX_TEMPERATURE_DDR)
	| (1 << ZONE_3_MAX_TEMPERATURE_DDR)
	| (1 << ZONE_4_MAX_TEMPERATURE_DDR)
	| (1 << ZONE_1_MIN_TEMPERATURE_DDR)
	| (1 << ZONE_2_MIN_TEMPERATURE_DDR)
	| (1 << ZONE_3_MIN_TEMPERATURE_DDR)
	| (1 << ZONE_4_MIN_TEMPERATURE_DDR);
		
	ZONE_TEMPERATURE_OUTPUT_PORT &=					// turn off ports for zones
	~( (1 << ZONE_1_MAX_TEMPERATURE_PORT)
	| (1 << ZONE_2_MAX_TEMPERATURE_PORT)
	| (1 << ZONE_3_MAX_TEMPERATURE_PORT)
	| (1 << ZONE_4_MAX_TEMPERATURE_PORT)
	| (1 << ZONE_1_MIN_TEMPERATURE_PORT)
	| (1 << ZONE_2_MIN_TEMPERATURE_PORT)
	| (1 << ZONE_3_MIN_TEMPERATURE_PORT)
	| (1 << ZONE_4_MIN_TEMPERATURE_PORT) );
	
	processZoneDHTData();		// receive initial sensor values
	
	// TODO: move to separate library: Timer2.c
	TCCR3B |= (1 << CS32) | (1 << CS30);	// setting prescale 1024
	TCCR3A = 0x00;
	TCNT3 = TIMER2_VALUE_FOR_FOUR_SECONDS;
	ETIMSK |= (1 << TOIE3);
	sei();
}

volatile ZoneDHTData * getZoneDHTData(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			return &zone1DhtData;
		case ZONE_2:
			return &zone2DhtData;
		case ZONE_3:
			return &zone3DhtData;
		case ZONE_4:
			return &zone4DhtData;
		default:
			return NULL;
	}
}

_Bool isTemperatureEnabled(Temperature *temperature) {
	return (temperature -> integralT + temperature -> decimalT + temperature -> integralHyst + temperature -> decimalHyst) != 0;// if all parameters is set to default values, then temp is disabled
}

static void processZoneDHTData() {
	
	if (zone1Settings.isEnabled) {
		readStatus = readDHT(DHT_SENSOR_TYPE, ZONE_1_DHT_READ_CHANNEL);
		mapZoneDhtData(&zone1DhtData, readStatus);
		checkMaxTempEvent(&zone1Settings, &zone1DhtData);
		checkMinTempEvent(&zone1Settings, &zone1DhtData);
	} else {
		switchOnMaxTempOutput(ZONE_1);
		switchOffMaxTempOutput(ZONE_1);
	}
	
	if (zone2Settings.isEnabled) {
		readStatus = readDHT(DHT_SENSOR_TYPE, ZONE_2_DHT_READ_CHANNEL);
		mapZoneDhtData(&zone2DhtData, readStatus);
		checkMaxTempEvent(&zone2Settings, &zone2DhtData);
		checkMinTempEvent(&zone2Settings, &zone2DhtData);
	} else {
		switchOnMaxTempOutput(ZONE_2);
		switchOffMaxTempOutput(ZONE_2);
	}
	
	if (zone3Settings.isEnabled) {
		readStatus = readDHT(DHT_SENSOR_TYPE, ZONE_3_DHT_READ_CHANNEL);
		mapZoneDhtData(&zone3DhtData, readStatus);
		checkMaxTempEvent(&zone3Settings, &zone3DhtData);
		checkMinTempEvent(&zone3Settings, &zone3DhtData);
	} else {
		switchOnMaxTempOutput(ZONE_3);
		switchOffMaxTempOutput(ZONE_3);
	}
	
	if (zone4Settings.isEnabled) {
		readStatus = readDHT(DHT_SENSOR_TYPE, ZONE_4_DHT_READ_CHANNEL);
		mapZoneDhtData(&zone4DhtData, readStatus);
		checkMaxTempEvent(&zone4Settings, &zone4DhtData);
		checkMinTempEvent(&zone4Settings, &zone4DhtData);
	} else {
		switchOnMaxTempOutput(ZONE_4);
		switchOffMaxTempOutput(ZONE_4);
	}
	
}

static DHTSensorStatus getSensorStatus(ReadStatus readStatus) {
	switch(readStatus) {
		case DHT_OK:
			return DHT_SENSOR_OK;
			
		case DHT_ERROR_CONNECT:
		case DHT_ERROR_TIMEOUT:
			return DHT_SENSOR_NOT_CONNECTED;
			
		case DHT_ERROR_ACK_L:
		case DHT_ERROR_ACK_H:
		case DHT_ERROR_CHECKSUM:
			return DHT_SENSOR_ERROR;
		
		default:
			return DHT_UNKNOWN_STATUS;
	}
}

static void mapZoneDhtData(volatile ZoneDHTData *zoneDhtData, volatile ReadStatus readStatus) {
	DHTSensorStatus sensorStatus = getSensorStatus(readStatus);
	zoneDhtData -> sensorStatus = sensorStatus;
	
	if (sensorStatus == DHT_SENSOR_OK) {
		zoneDhtData -> integralRH = getHumidityIntegralPart();
		zoneDhtData -> decimalRH = getHumidityDecimalPart();
		zoneDhtData -> integralT = getTemperatureIntegralPart();
		zoneDhtData -> decimalT = getTemperatureDecimalPart();
	}
}

static void checkMaxTempEvent(ZoneSettings *zoneSettings, volatile ZoneDHTData *zoneDhtData) {		// if now temperature is higher or equals than max temperature, then cooling system should be enabled
	if (zoneDhtData -> sensorStatus == DHT_SENSOR_OK && isTemperatureEnabled(&zoneSettings -> temperatureMax)) {
		
		if (!zoneDhtData -> isMaxTempEventOccured && isTempNowIsHigherOrEqualsThanMax(&zoneSettings -> temperatureMax, zoneDhtData)) {
			zoneDhtData -> isMaxTempEventOccured = true;
		}
		if (zoneDhtData -> isMaxTempEventOccured && isTempNowLessOrEqualsThanHysteresisMax(&zoneSettings -> temperatureMax, zoneDhtData)) {
			zoneDhtData -> isMaxTempEventOccured = false;
		}
		if (zoneDhtData -> isMaxTempEventOccured) {
			switchOnMaxTempOutput(zoneSettings -> zoneNumber);
		} else {
			switchOffMaxTempOutput(zoneSettings -> zoneNumber);
		}
	}
}

static void checkMinTempEvent(ZoneSettings *zoneSettings, volatile ZoneDHTData *zoneDhtData) {		// if now temperature is lower or equals than min temperature, then heating system should be enabled
	if (zoneDhtData -> sensorStatus == DHT_SENSOR_OK && isTemperatureEnabled(&zoneSettings -> temperatureMin)) {
		
		if (!zoneDhtData -> isMinTempEventOccured && isTempNowIsLessOrEqualsThanMin(&zoneSettings -> temperatureMin, zoneDhtData)) {
			zoneDhtData -> isMinTempEventOccured = true;
		}
		if (zoneDhtData -> isMinTempEventOccured && isTempNowHigherOrEqualsThanHysteresisMin(&zoneSettings -> temperatureMin, zoneDhtData)) {
			zoneDhtData -> isMinTempEventOccured = false;
		}
		if (zoneDhtData -> isMinTempEventOccured) {
			switchOnMinTempOutput(zoneSettings -> zoneNumber);
		} else {
			switchOffMinTempOutput(zoneSettings -> zoneNumber);
		}
	}
}

static _Bool isTempNowIsHigherOrEqualsThanMax(Temperature *temperature, volatile ZoneDHTData *zoneDhtData) {
	return (zoneDhtData -> integralT > temperature -> integralT) || ((zoneDhtData -> integralT == temperature -> integralT) && (zoneDhtData -> decimalT >= temperature -> decimalT));
}

static _Bool isTempNowLessOrEqualsThanHysteresisMax(Temperature *temperature, volatile ZoneDHTData *zoneDhtData) {
	return (zoneDhtData -> integralT < (temperature -> integralT - temperature -> integralHyst)) || 
	((zoneDhtData -> integralT == (temperature -> integralT - temperature -> integralHyst)) && (zoneDhtData -> decimalT <= (temperature -> decimalT - temperature -> decimalHyst)));
}

static void switchOnMaxTempOutput(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_1_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_2:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_2_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_3:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_3_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_4:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_4_MAX_TEMPERATURE_PORT);
			break;
	}
}

static void switchOffMaxTempOutput(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_1_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_2:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_2_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_3:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_3_MAX_TEMPERATURE_PORT);
			break;
		case ZONE_4:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_4_MAX_TEMPERATURE_PORT);
			break;
	}
}

static _Bool isTempNowIsLessOrEqualsThanMin(Temperature *temperature, volatile ZoneDHTData *zoneDhtData) {
	return (zoneDhtData -> integralT < temperature -> integralT) || ((zoneDhtData -> integralT == temperature -> integralT) && (zoneDhtData -> decimalT <= temperature -> decimalT));
}

static _Bool isTempNowHigherOrEqualsThanHysteresisMin(Temperature *temperature, volatile ZoneDHTData *zoneDhtData) {
	return (zoneDhtData -> integralT > (temperature -> integralT + temperature -> integralHyst)) || 
	((zoneDhtData -> integralT == (temperature -> integralT + temperature -> integralHyst)) && (zoneDhtData -> decimalT >= (temperature -> decimalT + temperature -> decimalHyst)));
}

static void switchOnMinTempOutput(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_1_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_2:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_2_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_3:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_3_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_4:
			ZONE_TEMPERATURE_OUTPUT_PORT |= (1 << ZONE_4_MIN_TEMPERATURE_PORT);
			break;
	}
}

static void switchOffMinTempOutput(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_1_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_2:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_2_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_3:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_3_MIN_TEMPERATURE_PORT);
			break;
		case ZONE_4:
			ZONE_TEMPERATURE_OUTPUT_PORT &= ~(1 << ZONE_4_MIN_TEMPERATURE_PORT);
			break;
	}
}