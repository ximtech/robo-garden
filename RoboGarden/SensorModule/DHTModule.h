#pragma once

#include "../Libs/DHT/DHT.h"
#include "../SettingsModule/ZoneSettings/ZoneCommonSettings.h"

#define DHT_SENSOR_TYPE DHT11

#define ZONE_1_DHT_READ_CHANNEL DHT_CHANNEL_1
#define ZONE_2_DHT_READ_CHANNEL DHT_CHANNEL_2
#define ZONE_3_DHT_READ_CHANNEL DHT_CHANNEL_3
#define ZONE_4_DHT_READ_CHANNEL DHT_CHANNEL_4

#define ZONE_TEMPERATURE_OUTPUT_DDR   DDRE
#define ZONE_TEMPERATURE_OUTPUT_PORT  PORTE

#define ZONE_1_MAX_TEMPERATURE_DDR DDE0
#define ZONE_2_MAX_TEMPERATURE_DDR DDE1
#define ZONE_3_MAX_TEMPERATURE_DDR DDE2
#define ZONE_4_MAX_TEMPERATURE_DDR DDE3
#define ZONE_1_MIN_TEMPERATURE_DDR DDE4
#define ZONE_2_MIN_TEMPERATURE_DDR DDE5
#define ZONE_3_MIN_TEMPERATURE_DDR DDE6
#define ZONE_4_MIN_TEMPERATURE_DDR DDE7

#define ZONE_1_MAX_TEMPERATURE_PORT PE0
#define ZONE_2_MAX_TEMPERATURE_PORT PE1
#define ZONE_3_MAX_TEMPERATURE_PORT PE2
#define ZONE_4_MAX_TEMPERATURE_PORT PE3
#define ZONE_1_MIN_TEMPERATURE_PORT PE4
#define ZONE_2_MIN_TEMPERATURE_PORT PE5
#define ZONE_3_MIN_TEMPERATURE_PORT PE6
#define ZONE_4_MIN_TEMPERATURE_PORT PE7


typedef enum DHTSensorStatus {
	DHT_SENSOR_OK,
	DHT_SENSOR_NOT_CONNECTED,
	DHT_SENSOR_ERROR,
	DHT_UNKNOWN_STATUS
} DHTSensorStatus;

typedef struct ZoneDHTData {
	DHTSensorStatus sensorStatus;
	uint8_t integralRH;
	uint8_t decimalRH;
	int8_t integralT;	// temperature can be negative, setting signed int
	uint8_t decimalT;
	_Bool isMaxTempEventOccured;
	_Bool isMinTempEventOccured;
} ZoneDHTData;


void initDHTHandler();
volatile ZoneDHTData * getZoneDHTData(ZoneNumber zoneNumber);
_Bool isTemperatureEnabled(Temperature *temperature);