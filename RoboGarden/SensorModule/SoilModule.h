#pragma once

#include "../Libs/ADC/ADC.h"
#include "../SettingsModule/ZoneSettings/ZoneCommonSettings.h"

#define ZONE_ADC_READ_CHANNEL_PIN DDF0
#define ZONE_ADC_READ_CHANNEL_PORT PF0

#define CHANEL_CONTROL_DDR  DDRG
#define CHANEL_CONTROL_PORT PORTG

#define S0_CONTROL_PIN     DDG0
#define S1_CONTROL_PIN     DDG1
#define S2_CONTROL_PIN     DDG2
#define S3_CONTROL_PIN	   DDG3
#define ENABLE_CONTROL_PIN DDG4

#define S0_CONTROL_PORT   PG0
#define S1_CONTROL_PORT   PG1
#define S2_CONTROL_PORT   PG2
#define S3_CONTROL_PORT   PG3
#define ENABLE_POWER_PORT PG4


void initSoilSensorHandler();
uint16_t readZoneSoilMoisureInPercentages(ZoneNumber zoneNumber);
_Bool isSoilSensorEnabled(ZoneNumber zoneNumber);

