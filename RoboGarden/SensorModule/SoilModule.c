#include "SoilModule.h"

#define CH_COUNT_PER_ZONE 4
#define MAX_ADC_VALUE 1023

static inline _Bool isSensorEnabled(uint8_t sensorCount);
static void readSensorADC(const uint8_t *zoneControlPins, uint16_t *muxReceivedValues, uint8_t sensorCount);
static uint8_t calculateAverageMoisture(uint16_t *muxReceivedValues, uint8_t sensorCount);
static inline uint8_t calculateMoisure(uint16_t adcValue);

void initSoilSensorHandler() {
	initADC();
	
	DDRF &= ~(1 << ZONE_ADC_READ_CHANNEL_PIN);
	PORTF &= ~(1 << ZONE_ADC_READ_CHANNEL_PORT);
	
	CHANEL_CONTROL_DDR = CHANEL_CONTROL_DDR 
	| (1 << S0_CONTROL_PIN) 
	| (1 << S1_CONTROL_PIN) 
	| (1 << S2_CONTROL_PIN) 
	| (1 << S3_CONTROL_PIN)
	| (1 << ENABLE_CONTROL_PIN);
	
	CHANEL_CONTROL_PORT = CHANEL_CONTROL_PORT
	& ~(1 << S0_CONTROL_PORT) 
	& ~(1 << S1_CONTROL_PORT) 
	& ~(1 << S2_CONTROL_PORT) 
	& ~(1 << S3_CONTROL_PORT)
	& ~(1 << ENABLE_POWER_PORT);
}

uint16_t readZoneSoilMoisureInPercentages(ZoneNumber zoneNumber) {// 74HC4067 Multiplexer has 16 channels, 4 channels for each zone
		uint16_t muxReceivedValues[CH_COUNT_PER_ZONE] = { [0 ... (CH_COUNT_PER_ZONE - 1)] = 0 };
			
		if (zoneNumber == ZONE_1) {
			static const uint8_t ZONE_1_CONTROL_PINS[] = { 0, 1, 2, 3 }; // ch0, ch1, ch2, ch3
			readSensorADC(ZONE_1_CONTROL_PINS, muxReceivedValues, zone1Settings.soilData.sensorCount);
			return calculateAverageMoisture(muxReceivedValues, zone1Settings.soilData.sensorCount);
		
		} else if (zoneNumber == ZONE_2) {
			static const uint8_t ZONE_2_CONTROL_PINS[] = { 4, 5, 6, 7 }; // ch4, ch5, ch6, ch7
			readSensorADC(ZONE_2_CONTROL_PINS, muxReceivedValues, zone2Settings.soilData.sensorCount);
			return calculateAverageMoisture(muxReceivedValues, zone2Settings.soilData.sensorCount);
			
		} else if (zoneNumber == ZONE_3) {
			static const uint8_t ZONE_3_CONTROL_PINS[] = { 8, 9, 10, 11 }; // ch8, ch9, ch10, ch11
			readSensorADC(ZONE_3_CONTROL_PINS, muxReceivedValues, zone3Settings.soilData.sensorCount);
			return calculateAverageMoisture(muxReceivedValues, zone3Settings.soilData.sensorCount);
			
		} else if (zoneNumber == ZONE_4) {
			static const uint8_t ZONE_4_CONTROL_PINS[] = { 12, 13, 14, 15 }; // ch12, ch13, ch14, ch15
			readSensorADC(ZONE_4_CONTROL_PINS, muxReceivedValues, zone4Settings.soilData.sensorCount);
			return calculateAverageMoisture(muxReceivedValues, zone4Settings.soilData.sensorCount);
			
		} else {
			return 0;
		}
}

_Bool isSoilSensorEnabled(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			return isSensorEnabled(zone1Settings.soilData.sensorCount);
		case ZONE_2:
			return isSensorEnabled(zone2Settings.soilData.sensorCount);
		case ZONE_3:
			return isSensorEnabled(zone3Settings.soilData.sensorCount);
		case ZONE_4:
			return isSensorEnabled(zone4Settings.soilData.sensorCount);
		default:
			return false;
	}
}

static inline _Bool isSensorEnabled(uint8_t sensorCount) {
	return (sensorCount > 0);
}

static void readSensorADC(const uint8_t *zoneControlPins, uint16_t *muxReceivedValues, uint8_t sensorCount) {
	CHANEL_CONTROL_PORT |= (1 << ENABLE_POWER_PORT);	// enable MUX Vcc supply
	for (uint8_t i = 0; i < sensorCount; i++) {			// collect ADC data from sensors
		CHANEL_CONTROL_PORT = zoneControlPins[i];
		muxReceivedValues[i] = getADC10BitValue(ZONE_ADC_READ_CHANNEL_PORT);
		_delay_ms(20);
	}
	CHANEL_CONTROL_PORT &= ~(1 << ENABLE_POWER_PORT);	// turn off MUX Vcc supply
}

static uint8_t calculateAverageMoisture(uint16_t *muxReceivedValues, uint8_t sensorCount) {
	uint16_t result = 0;
	for (uint8_t i = 0; i < sensorCount; i++) {
		result += calculateMoisure(muxReceivedValues[i]);
	}
	return (result / sensorCount);
}

static inline uint8_t calculateMoisure(uint16_t adcValue) {			// for ADC == 1023 -> will be 0% moisure
	return (100 - (((uint32_t)adcValue * 100) / MAX_ADC_VALUE));	// cast for preventing overflow of uint16
}
