#pragma once

#include "../SettingsModule/CommonSettings.h"
#include "../SettingsModule/DateTimeSetting/DateTimeSettings.h"
#include "../SettingsModule/ZoneSettings/OnOffSetting/ZoneOnOffSettings.h"
#include "../SettingsModule/ZoneSettings/TimerSetting/ZoneTimerSettings.h"
#include "../SettingsModule/ZoneSettings/TimerSetting/ZoneWeekDaySettings.h"
#include "../SettingsModule/ZoneSettings/TemperatureSettings/TemperatureSettings.h"
#include "../SettingsModule/ZoneSettings/DataView/ZoneDataView.h"
#include "../SettingsModule/ZoneSettings/SoilSettings/ZoneSoilSettings.h"

void initMenu(void (*backFunctPointer)());