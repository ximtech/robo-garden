#include "MenuModule.h"


void initMenu(void (*backFunctPointer)()) {
	
	setupMenu(clearLCD, printLCDString, goToXYLCD);
	setBackFunction(backFunctPointer);
	enableMenuArrowPointerAtPosition((LCD_COL_COUNT - 1), POINTER_TO_LEFT_SYMBOL_NUM, printLCDCustomCharacter);
	
	//---------------------- Main menu -------------------------------------------------
	MenuItem *mainMenu1 = createMenuItemWithNoSubMenus(L_MAIN_MENU, NULL);	// Main menu label only
	MenuItem *mainMenu2 = createMenuItemWithNoSubMenus(L_SET_DATE_TIME, setDateTime);
	MenuItem *mainMenu3 = createMenuItemWithSubMenus(L_SET_ZONE_1, 6);
	MenuItem *mainMenu4 = createMenuItemWithSubMenus(L_SET_ZONE_2, 6);
	MenuItem *mainMenu5 = createMenuItemWithSubMenus(L_SET_ZONE_3, 6);
	MenuItem *mainMenu6 = createMenuItemWithSubMenus(L_SET_ZONE_4, 6);
	addRootMenuItem(mainMenu1, 0);
	addRootMenuItem(mainMenu2, 1);
	addRootMenuItem(mainMenu3, 2);
	addRootMenuItem(mainMenu4, 3);
	addRootMenuItem(mainMenu5, 4);
	addRootMenuItem(mainMenu6, 5);

	//---------------------- Menu for zone 1 -------------------------------------------------
	MenuItem *zone1SubMenu0 = createMenuItemWithNoSubMenus(L_SET_ON_OFF, setOnOffForZone1);
	MenuItem *zone1SubMenu1 = createMenuItemWithSubMenus(L_SETUP_WATERING, 3);
	MenuItem *zone1SubMenu2 = createMenuItemWithSubMenus(L_SETUP_LIGHT, 3);
	MenuItem *zone1SubMenu3 = createMenuItemWithSubMenus(L_SETUP_TEMPERATURE, 2);
	MenuItem *zone1SubMenu4 = createMenuItemWithNoSubMenus(L_VIEW_STATUS, viewZone1SensorData);
	MenuItem *zone1SubMenu5 = createMenuItemWithNoSubMenus(L_SETUP_SOIL_SENSOR, setZone1SoilSettings);
	
	addSubMenuItem(mainMenu3, zone1SubMenu0, 0);
	addSubMenuItem(mainMenu3, zone1SubMenu1, 1);
	addSubMenuItem(mainMenu3, zone1SubMenu2, 2);
	addSubMenuItem(mainMenu3, zone1SubMenu3, 3);
	addSubMenuItem(mainMenu3, zone1SubMenu4, 4);
	addSubMenuItem(mainMenu3, zone1SubMenu5, 5);
	
	// Temperature settings
	MenuItem *zone1MaxTemperature = createMenuItemWithNoSubMenus(L_SETUP_MAX_TEMPERATURE, setMaxTemperatureForZone1);
	MenuItem *zone1MinTemperature = createMenuItemWithNoSubMenus(L_SETUP_MIN_TEMPERATURE, setMinTemperatureForZone1);
	addSubMenuItem(zone1SubMenu3, zone1MaxTemperature, 0);
	addSubMenuItem(zone1SubMenu3, zone1MinTemperature, 1);
	
	// Zone 1 timer settings for watering
	MenuItem *zone1WateringTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone1WateringTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone1WateringTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone1SubMenu1, zone1WateringTimerMenu1, 0);
	addSubMenuItem(zone1SubMenu1, zone1WateringTimerMenu2, 1);
	addSubMenuItem(zone1SubMenu1, zone1WateringTimerMenu3, 2);

	// Timer 1 settings for watering
	MenuItem *zone1WateringTimer1Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1WateringForZone1);
	MenuItem *zone1WateringTimer1WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1WateringWeekdaysForZone1);
	addSubMenuItem(zone1WateringTimerMenu1, zone1WateringTimer1Time, 0);
	addSubMenuItem(zone1WateringTimerMenu1, zone1WateringTimer1WeekDay, 1);
	
	// Timer 2 settings for watering
	MenuItem *zone1WateringTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2WateringForZone1);
	MenuItem *zone1WateringTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2WateringWeekdaysForZone1);
	addSubMenuItem(zone1WateringTimerMenu2, zone1WateringTimer2Time, 0);
	addSubMenuItem(zone1WateringTimerMenu2, zone1WateringTimer2WeekDay, 1);
	
	// Timer 3 settings for watering
	MenuItem *zone1WateringTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3WateringForZone1);
	MenuItem *zone1WateringTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3WateringWeekdaysForZone1);
	addSubMenuItem(zone1WateringTimerMenu3, zone1WateringTimer3Time, 0);
	addSubMenuItem(zone1WateringTimerMenu3, zone1WateringTimer3WeekDay, 1);
	
	// Zone 1 timer settings for light
	MenuItem *zone1LightTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone1LightTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone1LightTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone1SubMenu2, zone1LightTimerMenu1, 0);
	addSubMenuItem(zone1SubMenu2, zone1LightTimerMenu2, 1);
	addSubMenuItem(zone1SubMenu2, zone1LightTimerMenu3, 2);
	
	// Timer 1 settings for light
	MenuItem *zone1LightTimer1Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1LightForZone1);
	MenuItem *zone1LightTimer1WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1LightWeekdaysForZone1);
	addSubMenuItem(zone1LightTimerMenu1, zone1LightTimer1Time, 0);
	addSubMenuItem(zone1LightTimerMenu1, zone1LightTimer1WeekDay, 1);
	
	// Timer 2 settings for light
	MenuItem *zone1LightTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2LightForZone1);
	MenuItem *zone1LightTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2LightWeekdaysForZone1);
	addSubMenuItem(zone1LightTimerMenu2, zone1LightTimer2Time, 0);
	addSubMenuItem(zone1LightTimerMenu2, zone1LightTimer2WeekDay, 1);
	
	// Timer 3 settings for light
	MenuItem *zone1LightTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3LightForZone1);
	MenuItem *zone1LightTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3LightWeekdaysForZone1);
	addSubMenuItem(zone1LightTimerMenu3, zone1LightTimer3Time, 0);
	addSubMenuItem(zone1LightTimerMenu3, zone1LightTimer3WeekDay, 1);
	
	
	//---------------------- Menu for zone 2 -------------------------------------------------
	MenuItem *zone2SubMenu0 = createMenuItemWithNoSubMenus(L_SET_ON_OFF, setOnOffForZone2);
	MenuItem *zone2SubMenu1 = createMenuItemWithSubMenus(L_SETUP_WATERING, 3);
	MenuItem *zone2SubMenu2 = createMenuItemWithSubMenus(L_SETUP_LIGHT, 3);
	MenuItem *zone2SubMenu3 = createMenuItemWithSubMenus(L_SETUP_TEMPERATURE, 2);
	MenuItem *zone2SubMenu4 = createMenuItemWithNoSubMenus(L_VIEW_STATUS, viewZone2SensorData);
	MenuItem *zone2SubMenu5 = createMenuItemWithNoSubMenus(L_SETUP_SOIL_SENSOR, setZone2SoilSettings);
	addSubMenuItem(mainMenu4, zone2SubMenu0, 0);
	addSubMenuItem(mainMenu4, zone2SubMenu1, 1);
	addSubMenuItem(mainMenu4, zone2SubMenu2, 2);
	addSubMenuItem(mainMenu4, zone2SubMenu3, 3);
	addSubMenuItem(mainMenu4, zone2SubMenu4, 4);
	addSubMenuItem(mainMenu4, zone2SubMenu5, 5);
	
	// Temperature settings
	MenuItem *zone2MaxTemperature = createMenuItemWithNoSubMenus(L_SETUP_MAX_TEMPERATURE, setMaxTemperatureForZone2);
	MenuItem *zone2MinTemperature = createMenuItemWithNoSubMenus(L_SETUP_MIN_TEMPERATURE, setMinTemperatureForZone2);
	addSubMenuItem(zone2SubMenu3, zone2MaxTemperature, 0);
	addSubMenuItem(zone2SubMenu3, zone2MinTemperature, 1);
	
	// Zone 2 timer settings
	MenuItem *zone2WateringTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone2WateringTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone2WateringTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone2SubMenu1, zone2WateringTimerMenu1, 0);
	addSubMenuItem(zone2SubMenu1, zone2WateringTimerMenu2, 1);
	addSubMenuItem(zone2SubMenu1, zone2WateringTimerMenu3, 2);

	// Timer 1 settings for watering
	MenuItem *zone2WateringTimer1Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1WateringForZone2);
	MenuItem *zone2WateringTimer1WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1WateringWeekdaysForZone2);
	addSubMenuItem(zone2WateringTimerMenu1, zone2WateringTimer1Time, 0);
	addSubMenuItem(zone2WateringTimerMenu1, zone2WateringTimer1WeekDay, 1);
	
	// Timer 2 settings for watering
	MenuItem *zone2WateringTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2WateringForZone2);
	MenuItem *zone2WateringTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2WateringWeekdaysForZone2);
	addSubMenuItem(zone2WateringTimerMenu2, zone2WateringTimer2Time, 0);
	addSubMenuItem(zone2WateringTimerMenu2, zone2WateringTimer2WeekDay, 1);
	
	// Timer 3 settings for watering
	MenuItem *zone2WateringTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3WateringForZone2);
	MenuItem *zone2WateringTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3WateringWeekdaysForZone2);
	addSubMenuItem(zone2WateringTimerMenu3, zone2WateringTimer3Time, 0);
	addSubMenuItem(zone2WateringTimerMenu3, zone2WateringTimer3WeekDay, 1);
	
	// Zone 2 timer settings for light
	MenuItem *zone2LightTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone2LightTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone2LightTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone2SubMenu2, zone2LightTimerMenu1, 0);
	addSubMenuItem(zone2SubMenu2, zone2LightTimerMenu2, 1);
	addSubMenuItem(zone2SubMenu2, zone2LightTimerMenu3, 2);
	
	// Timer 1 settings for light
	MenuItem *zone2LightTimerTime = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1LightForZone2);
	MenuItem *zone2LightTimerWeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1LightWeekdaysForZone2);
	addSubMenuItem(zone2LightTimerMenu1, zone2LightTimerTime, 0);
	addSubMenuItem(zone2LightTimerMenu1, zone2LightTimerWeekDay, 1);
	
	// Timer 2 settings for light
	MenuItem *zone2LightTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2LightForZone2);
	MenuItem *zone2LightTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2LightWeekdaysForZone2);
	addSubMenuItem(zone2LightTimerMenu2, zone2LightTimer2Time, 0);
	addSubMenuItem(zone2LightTimerMenu2, zone2LightTimer2WeekDay, 1);
	
	// Timer 3 settings for light
	MenuItem *zone2LightTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3LightForZone2);
	MenuItem *zone2LightTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3LightWeekdaysForZone2);
	addSubMenuItem(zone2LightTimerMenu3, zone2LightTimer3Time, 0);
	addSubMenuItem(zone2LightTimerMenu3, zone2LightTimer3WeekDay, 1);
	
	//---------------------- Menu for zone 3 -------------------------------------------------
	MenuItem *zone3SubMenu0 = createMenuItemWithNoSubMenus(L_SET_ON_OFF, setOnOffForZone3);
	MenuItem *zone3SubMenu1 = createMenuItemWithSubMenus(L_SETUP_WATERING, 3);
	MenuItem *zone3SubMenu2 = createMenuItemWithSubMenus(L_SETUP_LIGHT, 3);
	MenuItem *zone3SubMenu3 = createMenuItemWithSubMenus(L_SETUP_TEMPERATURE, 2);
	MenuItem *zone3SubMenu4 = createMenuItemWithNoSubMenus(L_VIEW_STATUS, viewZone3SensorData);
	MenuItem *zone3SubMenu5 = createMenuItemWithNoSubMenus(L_SETUP_SOIL_SENSOR, setZone3SoilSettings);
	addSubMenuItem(mainMenu5, zone3SubMenu0, 0);
	addSubMenuItem(mainMenu5, zone3SubMenu1, 1);
	addSubMenuItem(mainMenu5, zone3SubMenu2, 2);
	addSubMenuItem(mainMenu5, zone3SubMenu3, 3);
	addSubMenuItem(mainMenu5, zone3SubMenu4, 4);
	addSubMenuItem(mainMenu5, zone3SubMenu5, 5);
	
	// Temperature settings
	MenuItem *zone3MaxTemperature = createMenuItemWithNoSubMenus(L_SETUP_MAX_TEMPERATURE, setMaxTemperatureForZone3);
	MenuItem *zone3MinTemperature = createMenuItemWithNoSubMenus(L_SETUP_MIN_TEMPERATURE, setMinTemperatureForZone3);
	addSubMenuItem(zone3SubMenu3, zone3MaxTemperature, 0);
	addSubMenuItem(zone3SubMenu3, zone3MinTemperature, 1);
	
	// Zone 3 timer settings
	MenuItem *zone3WateringTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone3WateringTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone3WateringTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone3SubMenu2, zone3WateringTimerMenu1, 0);
	addSubMenuItem(zone3SubMenu2, zone3WateringTimerMenu2, 1);
	addSubMenuItem(zone3SubMenu2, zone3WateringTimerMenu3, 2);
	
	// Timer 1 settings for watering
	MenuItem *zone3WateringTimer1Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1WateringForZone3);
	MenuItem *zone3WateringTimer1WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1WateringWeekdaysForZone3);
	addSubMenuItem(zone3WateringTimerMenu1, zone3WateringTimer1Time, 0);
	addSubMenuItem(zone3WateringTimerMenu1, zone3WateringTimer1WeekDay, 1);
	
	// Timer 2 settings for watering
	MenuItem *zone3WateringTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2WateringForZone3);
	MenuItem *zone3WateringTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2WateringWeekdaysForZone3);
	addSubMenuItem(zone3WateringTimerMenu2, zone3WateringTimer2Time, 0);
	addSubMenuItem(zone3WateringTimerMenu2, zone3WateringTimer2WeekDay, 1);
	
	// Timer 3 settings for watering
	MenuItem *zone3WateringTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3WateringForZone3);
	MenuItem *zone3WateringTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3WateringWeekdaysForZone3);
	addSubMenuItem(zone3WateringTimerMenu3, zone3WateringTimer3Time, 0);
	addSubMenuItem(zone3WateringTimerMenu3, zone3WateringTimer3WeekDay, 1);
	
	// Zone 3 timer settings for light
	MenuItem *zone3LightTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone3LightTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone3LightTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu1, 0);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu2, 1);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu3, 2);
	
	// Timer 1 settings for light
	MenuItem *zone3LightTimerTime = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1LightForZone3);
	MenuItem *zone3LightTimerWeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1LightWeekdaysForZone3);
	addSubMenuItem(zone3LightTimerMenu1, zone3LightTimerTime, 0);
	addSubMenuItem(zone3LightTimerMenu1, zone3LightTimerWeekDay, 1);
	
	// Timer 2 settings for light
	MenuItem *zone3LightTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2LightForZone3);
	MenuItem *zone3LightTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2LightWeekdaysForZone3);
	addSubMenuItem(zone3LightTimerMenu2, zone3LightTimer2Time, 0);
	addSubMenuItem(zone3LightTimerMenu2, zone3LightTimer2WeekDay, 1);
	
	// Timer 3 settings for light
	MenuItem *zone3LightTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3LightForZone3);
	MenuItem *zone3LightTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3LightWeekdaysForZone3);
	addSubMenuItem(zone3LightTimerMenu3, zone3LightTimer3Time, 0);
	addSubMenuItem(zone3LightTimerMenu3, zone3LightTimer3WeekDay, 1);
	
	//---------------------- Menu for zone 4 -------------------------------------------------
	MenuItem *zone4SubMenu0 = createMenuItemWithNoSubMenus(L_SET_ON_OFF, setOnOffForZone4);
	MenuItem *zone4SubMenu1 = createMenuItemWithSubMenus(L_SETUP_WATERING, 3);
	MenuItem *zone4SubMenu2 = createMenuItemWithSubMenus(L_SETUP_LIGHT, 3);
	MenuItem *zone4SubMenu3 = createMenuItemWithSubMenus(L_SETUP_TEMPERATURE, 2);
	MenuItem *zone4SubMenu4 = createMenuItemWithNoSubMenus(L_VIEW_STATUS, viewZone4SensorData);
	MenuItem *zone4SubMenu5 = createMenuItemWithNoSubMenus(L_SETUP_SOIL_SENSOR, setZone4SoilSettings);
	addSubMenuItem(mainMenu6, zone4SubMenu0, 0);
	addSubMenuItem(mainMenu6, zone4SubMenu1, 1);
	addSubMenuItem(mainMenu6, zone4SubMenu2, 2);
	addSubMenuItem(mainMenu6, zone4SubMenu3, 3);
	addSubMenuItem(mainMenu6, zone4SubMenu4, 4);
	addSubMenuItem(mainMenu6, zone4SubMenu5, 5);
	
	// Temperature settings
	MenuItem *zone4MaxTemperature = createMenuItemWithNoSubMenus(L_SETUP_MAX_TEMPERATURE, setMaxTemperatureForZone4);
	MenuItem *zone4MinTemperature = createMenuItemWithNoSubMenus(L_SETUP_MIN_TEMPERATURE, setMinTemperatureForZone4);
	addSubMenuItem(zone4SubMenu3, zone4MaxTemperature, 0);
	addSubMenuItem(zone4SubMenu3, zone4MinTemperature, 1);
	
	// Zone 3 timer settings
	MenuItem *zone4WateringTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone4WateringTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone4WateringTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone4SubMenu2, zone4WateringTimerMenu1, 0);
	addSubMenuItem(zone4SubMenu2, zone4WateringTimerMenu2, 1);
	addSubMenuItem(zone4SubMenu2, zone4WateringTimerMenu3, 2);
	
	// Timer 1 settings for watering
	MenuItem *zone4WateringTimer1Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1WateringForZone4);
	MenuItem *zone4WateringTimer1WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1WateringWeekdaysForZone4);
	addSubMenuItem(zone4WateringTimerMenu1, zone4WateringTimer1Time, 0);
	addSubMenuItem(zone4WateringTimerMenu1, zone4WateringTimer1WeekDay, 1);
	
	// Timer 2 settings for watering
	MenuItem *zone4WateringTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2WateringForZone4);
	MenuItem *zone4WateringTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2WateringWeekdaysForZone4);
	addSubMenuItem(zone4WateringTimerMenu2, zone4WateringTimer2Time, 0);
	addSubMenuItem(zone4WateringTimerMenu2, zone4WateringTimer2WeekDay, 1);
	
	// Timer 3 settings for watering
	MenuItem *zone4WateringTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3WateringForZone4);
	MenuItem *zone4WateringTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3WateringWeekdaysForZone4);
	addSubMenuItem(zone4WateringTimerMenu3, zone4WateringTimer3Time, 0);
	addSubMenuItem(zone4WateringTimerMenu3, zone4WateringTimer3WeekDay, 1);
	
	// Zone 3 timer settings for light
	MenuItem *zone4LightTimerMenu1 = createMenuItemWithSubMenus(L_SETUP_TIMER_1, 2);
	MenuItem *zone4LightTimerMenu2 = createMenuItemWithSubMenus(L_SETUP_TIMER_2, 2);
	MenuItem *zone4LightTimerMenu3 = createMenuItemWithSubMenus(L_SETUP_TIMER_3, 2);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu1, 0);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu2, 1);
	addSubMenuItem(zone3SubMenu3, zone3LightTimerMenu3, 2);
	
	// Timer 1 settings for light
	MenuItem *zone4LightTimerTime = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer1LightForZone4);
	MenuItem *zone4LightTimerWeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer1LightWeekdaysForZone4);
	addSubMenuItem(zone4LightTimerMenu1, zone4LightTimerTime, 0);
	addSubMenuItem(zone4LightTimerMenu1, zone4LightTimerWeekDay, 1);
	
	// Timer 2 settings for light
	MenuItem *zone4LightTimer2Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer2LightForZone4);
	MenuItem *zone4LightTimer2WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer2LightWeekdaysForZone4);
	addSubMenuItem(zone4LightTimerMenu2, zone4LightTimer2Time, 0);
	addSubMenuItem(zone4LightTimerMenu2, zone4LightTimer2WeekDay, 1);
	
	// Timer 3 settings for light
	MenuItem *zone4LightTimer3Time = createMenuItemWithNoSubMenus(L_SETUP_TIMER_TIME, setTimer3LightForZone4);
	MenuItem *zone4LightTimer3WeekDay = createMenuItemWithNoSubMenus(L_SETUP_TIMER_WEEK_DAY, setTimer3LightWeekdaysForZone4);
	addSubMenuItem(zone4LightTimerMenu3, zone4LightTimer3Time, 0);
	addSubMenuItem(zone4LightTimerMenu3, zone4LightTimer3WeekDay, 1);
}