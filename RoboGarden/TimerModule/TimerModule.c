#include "TimerModule.h"
#include <avr/interrupt.h>

#define TIMER1_VALUE_FOR_ONE_SECOND 35000		//31248 this value calculated for one second, then aligned(experimentally) to work properly with RTC

volatile LocalDateTime *localDateTime;

volatile TimerEvent timerEventList[] = {
	{ false, ZONE_1, TIMER1_WATERING, 0, 0 },	// 0
	{ false, ZONE_1, TIMER2_WATERING, 0, 0 },	// 1
	{ false, ZONE_1, TIMER3_WATERING, 0, 0 },	// 2
	{ false, ZONE_1, TIMER1_LIGHT, 0, 0 },		// 3
	{ false, ZONE_1, TIMER2_LIGHT, 0, 0 },		// 4
	{ false, ZONE_1, TIMER3_LIGHT, 0, 0 },		// 5
	
	{ false, ZONE_2, TIMER1_WATERING, 0, 0 },	// 6
	{ false, ZONE_2, TIMER2_WATERING, 0, 0 },	// 7
	{ false, ZONE_2, TIMER3_WATERING, 0, 0 },	// 8
	{ false, ZONE_2, TIMER1_LIGHT, 0, 0 },		// 9
	{ false, ZONE_2, TIMER2_LIGHT, 0, 0 },		// 10
	{ false, ZONE_2, TIMER3_LIGHT, 0, 0 },		// 11
		
	{ false, ZONE_3, TIMER1_WATERING, 0, 0 },	// 12
	{ false, ZONE_3, TIMER2_WATERING, 0, 0 },	// 13
	{ false, ZONE_3, TIMER3_WATERING, 0, 0 },	// 14
	{ false, ZONE_3, TIMER1_LIGHT, 0, 0 },		// 15
	{ false, ZONE_3, TIMER2_LIGHT, 0, 0 },		// 16
	{ false, ZONE_3, TIMER3_LIGHT, 0, 0 },		// 17
		
	{ false, ZONE_4, TIMER1_WATERING, 0, 0 },	// 18
	{ false, ZONE_4, TIMER2_WATERING, 0, 0 },	// 19
	{ false, ZONE_4, TIMER3_WATERING, 0, 0 },	// 20
	{ false, ZONE_4, TIMER1_LIGHT, 0, 0 },		// 21
	{ false, ZONE_4, TIMER2_LIGHT, 0, 0 },		// 22
	{ false, ZONE_4, TIMER3_LIGHT, 0, 0 }		// 23
};

static _Bool isZoneTimerEventProcessed(Timer *startTimer, Timer *endTimer, _Bool *weekDays, uint8_t eventListIndex);
static void disableWateringEvent(ZoneNumber zoneNumber);
static void disableAllZoneEvents(ZoneNumber zoneNumber);
static void disableZoneOutputs(ZoneNumber zoneNumber);
static _Bool isSoilMoistureCapableForWatering(ZoneSettings *zoneSettings);


ISR(TIMER1_OVF_vect) {
	
	localDateTime = getRTCdateTime();
	
	if (zone1Settings.isEnabled) {
		if ((isZoneTimerEventProcessed(&zone1Settings.waterTimer1.startTimer, &zone1Settings.waterTimer1.endTimer, zone1Settings.waterTimer1.weekDays, 0) ||
			isZoneTimerEventProcessed(&zone1Settings.waterTimer2.startTimer, &zone1Settings.waterTimer2.endTimer, zone1Settings.waterTimer2.weekDays, 1)  ||
			isZoneTimerEventProcessed(&zone1Settings.waterTimer3.startTimer, &zone1Settings.waterTimer3.endTimer, zone1Settings.waterTimer3.weekDays, 2)) && 
			(isSoilMoistureCapableForWatering(&zone1Settings))) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_1_WATER_TIMER_PORT);
		} else {
			disableWateringEvent(zone1Settings.zoneNumber);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_1_WATER_TIMER_PORT);
		}
		
		if (isZoneTimerEventProcessed(&zone1Settings.lightTimer1.startTimer, &zone1Settings.lightTimer1.endTimer, zone1Settings.lightTimer1.weekDays, 3) ||
			isZoneTimerEventProcessed(&zone1Settings.lightTimer2.startTimer, &zone1Settings.lightTimer2.endTimer, zone1Settings.lightTimer2.weekDays, 4) ||
			isZoneTimerEventProcessed(&zone1Settings.lightTimer3.startTimer, &zone1Settings.lightTimer3.endTimer, zone1Settings.lightTimer3.weekDays, 5)) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_1_LIGHT_TIMER_PORT);
		} else {
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_1_LIGHT_TIMER_PORT);
		}
	} else {
		disableAllZoneEvents(zone1Settings.zoneNumber);
	}
	
	if (zone2Settings.isEnabled) {
		if ((isZoneTimerEventProcessed(&zone2Settings.waterTimer1.startTimer, &zone2Settings.waterTimer1.endTimer, zone2Settings.waterTimer1.weekDays, 6) ||
			isZoneTimerEventProcessed(&zone2Settings.waterTimer2.startTimer, &zone2Settings.waterTimer2.endTimer, zone2Settings.waterTimer2.weekDays, 7)  ||
			isZoneTimerEventProcessed(&zone2Settings.waterTimer3.startTimer, &zone2Settings.waterTimer3.endTimer, zone2Settings.waterTimer3.weekDays, 8)) &&
			(isSoilMoistureCapableForWatering(&zone1Settings))) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_2_WATER_TIMER_PORT);		
		} else {
			disableWateringEvent(zone2Settings.zoneNumber);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_2_WATER_TIMER_PORT);
		}	
				
		if (isZoneTimerEventProcessed(&zone2Settings.lightTimer1.startTimer, &zone2Settings.lightTimer1.endTimer, zone2Settings.lightTimer1.weekDays, 9)  ||
			isZoneTimerEventProcessed(&zone2Settings.lightTimer2.startTimer, &zone2Settings.lightTimer2.endTimer, zone2Settings.lightTimer2.weekDays, 10) ||
			isZoneTimerEventProcessed(&zone2Settings.lightTimer3.startTimer, &zone2Settings.lightTimer3.endTimer, zone2Settings.lightTimer3.weekDays, 11)) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_2_LIGHT_TIMER_PORT);	
		} else {
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_2_LIGHT_TIMER_PORT);
		}
	} else {
		disableAllZoneEvents(zone2Settings.zoneNumber);
	}
	
	if (zone3Settings.isEnabled) {
		if ((isZoneTimerEventProcessed(&zone3Settings.waterTimer1.startTimer, &zone3Settings.waterTimer1.endTimer, zone3Settings.waterTimer1.weekDays, 12) ||
			isZoneTimerEventProcessed(&zone3Settings.waterTimer2.startTimer, &zone3Settings.waterTimer2.endTimer, zone3Settings.waterTimer2.weekDays, 13)  ||
			isZoneTimerEventProcessed(&zone3Settings.waterTimer3.startTimer, &zone3Settings.waterTimer3.endTimer, zone3Settings.waterTimer3.weekDays, 14)) && 
			(isSoilMoistureCapableForWatering(&zone3Settings))) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_3_WATER_TIMER_PORT);
		} else {
			disableWateringEvent(zone3Settings.zoneNumber);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_3_WATER_TIMER_PORT);
		}
		
		if (isZoneTimerEventProcessed(&zone3Settings.lightTimer1.startTimer, &zone3Settings.lightTimer1.endTimer, zone3Settings.lightTimer1.weekDays, 15)  ||
			isZoneTimerEventProcessed(&zone3Settings.lightTimer2.startTimer, &zone3Settings.lightTimer2.endTimer, zone3Settings.lightTimer2.weekDays, 16) ||
			isZoneTimerEventProcessed(&zone3Settings.lightTimer3.startTimer, &zone3Settings.lightTimer3.endTimer, zone3Settings.lightTimer3.weekDays, 17)) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_3_LIGHT_TIMER_PORT);
		} else {
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_3_LIGHT_TIMER_PORT);
		}
	} else {
		disableAllZoneEvents(zone3Settings.zoneNumber);
	}
		
	if (zone4Settings.isEnabled) {
		if ((isZoneTimerEventProcessed(&zone4Settings.waterTimer1.startTimer, &zone4Settings.waterTimer1.endTimer, zone4Settings.waterTimer1.weekDays, 18) ||
			isZoneTimerEventProcessed(&zone4Settings.waterTimer2.startTimer, &zone4Settings.waterTimer2.endTimer, zone4Settings.waterTimer2.weekDays, 19)  ||
			isZoneTimerEventProcessed(&zone4Settings.waterTimer3.startTimer, &zone4Settings.waterTimer3.endTimer, zone4Settings.waterTimer3.weekDays, 20)) &&
			(isSoilMoistureCapableForWatering(&zone4Settings))) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_3_WATER_TIMER_PORT);
		} else {
			disableWateringEvent(zone4Settings.zoneNumber);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_3_WATER_TIMER_PORT);
		}
		
		if (isZoneTimerEventProcessed(&zone4Settings.lightTimer1.startTimer, &zone4Settings.lightTimer1.endTimer, zone4Settings.lightTimer1.weekDays, 21)  ||
			isZoneTimerEventProcessed(&zone4Settings.lightTimer2.startTimer, &zone4Settings.lightTimer2.endTimer, zone4Settings.lightTimer2.weekDays, 22) ||
			isZoneTimerEventProcessed(&zone4Settings.lightTimer3.startTimer, &zone4Settings.lightTimer3.endTimer, zone4Settings.lightTimer3.weekDays, 23)) {
			ZONE_TIMER_OUTPUT_PORT |= (1 << ZONE_4_LIGHT_TIMER_PORT);
		} else {
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_4_LIGHT_TIMER_PORT);
		}
	} else {
		disableAllZoneEvents(zone4Settings.zoneNumber);
	}
		
	TCNT1 = TIMER1_VALUE_FOR_ONE_SECOND;
}


void initEventHandler() {
	
	ZONE_TIMER_OUTPUT_DDR = ZONE_TIMER_OUTPUT_DDR	// configure as output for zones
	| (1 << ZONE_1_WATER_TIMER_DDR)
	| (1 << ZONE_1_LIGHT_TIMER_DDR)
	| (1 << ZONE_2_WATER_TIMER_DDR)
	| (1 << ZONE_2_LIGHT_TIMER_DDR)
	| (1 << ZONE_3_WATER_TIMER_DDR)
	| (1 << ZONE_3_LIGHT_TIMER_DDR)
	| (1 << ZONE_4_WATER_TIMER_DDR)
	| (1 << ZONE_4_LIGHT_TIMER_DDR);
	
	ZONE_TIMER_OUTPUT_PORT &=					// turn off ports for zones
	~( (1 << ZONE_1_WATER_TIMER_PORT)
	 | (1 << ZONE_1_LIGHT_TIMER_PORT)
	 | (1 << ZONE_2_WATER_TIMER_PORT)
	 | (1 << ZONE_2_LIGHT_TIMER_PORT) 
	 | (1 << ZONE_3_WATER_TIMER_PORT)
	 | (1 << ZONE_3_LIGHT_TIMER_PORT)
	 | (1 << ZONE_4_WATER_TIMER_PORT)
	 | (1 << ZONE_4_LIGHT_TIMER_PORT) );

	localDateTime = getRTCdateTime();
	
	// TODO: move to separate library: Timer1.c
	TCCR1B |= (1 << CS12);	// setting prescale 256
	TCCR1A = 0x00;
	TCNT1 = TIMER1_VALUE_FOR_ONE_SECOND;
	TIMSK |= (1 << TOIE1);
	sei();
}

_Bool isNewEventOccurred() {
	for (uint8_t i = 0; i < TIMER_EVENT_LIST_SIZE; i++) {
		if (timerEventList[i].isEventOccured) {
			return true;
		}
	}
	return false;
}

volatile TimerEvent * getTimerEventList() {
	return timerEventList;
}

volatile LocalDateTime * getVolatileLocalDateTime() {
	return localDateTime;
}

_Bool isZoneTimersEnabled(Timer *startTimer, Timer *endTimer) {
	return startTimer -> hours   != 0 ||
		   startTimer -> minutes != 0 ||
		   startTimer -> seconds != 0 ||
		   endTimer -> hours	 != 0 ||
		   endTimer ->  minutes  != 0 ||
		   endTimer -> seconds   != 0;
}

_Bool isTimeEventOccured(Timer *startTimer, Timer *endTimer) {
	uint32_t startTimerSeconds = getSeconds(startTimer -> hours, startTimer -> minutes, startTimer -> seconds);
	uint32_t endTimerSeconds = getSeconds(endTimer -> hours, endTimer -> minutes, endTimer -> seconds);
	uint32_t nowTimeSeconds = getSeconds(localDateTime -> hour, localDateTime -> minute, localDateTime -> second);
	return (nowTimeSeconds >= startTimerSeconds) && (nowTimeSeconds <= endTimerSeconds);
}

static _Bool isZoneTimerEventProcessed(Timer *startTimer, Timer *endTimer, _Bool *weekDays, uint8_t eventListIndex) {
	if (isZoneTimersEnabled(startTimer, endTimer)) {
		if (isTimeEventOccured(startTimer, endTimer) && isWeekDayTimerEventOccured(weekDays)) {
			uint32_t totalSecondsInEndTimer = getSeconds(endTimer -> hours, endTimer -> minutes, endTimer -> seconds);
			uint32_t totalSecondsNow = getSeconds(localDateTime -> hour, localDateTime -> minute, localDateTime -> second);
			timerEventList[eventListIndex].isEventOccured = true;
			timerEventList[eventListIndex].secondsPassed = totalSecondsNow;
			timerEventList[eventListIndex].totalSecondsDuration = totalSecondsInEndTimer;
			return true;
		} else {
			timerEventList[eventListIndex].isEventOccured = false;
			timerEventList[eventListIndex].secondsPassed = 0;
			return false;
		}
	}
	return false;
}

_Bool isWeekDayTimerEventOccured(_Bool *weekDays) {
	return weekDays[localDateTime->weekDay - 1] == true;
}

static void disableWateringEvent(ZoneNumber zoneNumber) {
	for (uint8_t i = 0; i < TIMER_EVENT_LIST_SIZE; i++) {
		_Bool isWaterTimer = timerEventList[i].timerType == TIMER1_WATERING || 
							 timerEventList[i].timerType == TIMER2_WATERING || 
							 timerEventList[i].timerType == TIMER3_WATERING;
							 
		if (isWaterTimer && timerEventList[i].isEventOccured) {
			timerEventList[i].isEventOccured = false;
			timerEventList[i].secondsPassed = 0;
		}
	}
}

static void disableAllZoneEvents(ZoneNumber zoneNumber) {	// if zone is disabled, turn off all events and disable outputs
	for (uint8_t i = 0; i < TIMER_EVENT_LIST_SIZE; i++) {	// find active events in list
		if (timerEventList[i].zoneNumber == zoneNumber && timerEventList[i].isEventOccured) {
			timerEventList[i].isEventOccured = false;
			timerEventList[i].secondsPassed = 0;
		}
	}
	disableZoneOutputs(zoneNumber);
}

static void disableZoneOutputs(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1:
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_1_WATER_TIMER_PORT);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_1_LIGHT_TIMER_PORT);
			break;
		case ZONE_2:
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_2_WATER_TIMER_PORT);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_2_LIGHT_TIMER_PORT);
			break;
		case ZONE_3:
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_3_WATER_TIMER_PORT);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_3_LIGHT_TIMER_PORT);
			break;
		case ZONE_4:
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_4_WATER_TIMER_PORT);
			ZONE_TIMER_OUTPUT_PORT &= ~(1 << ZONE_4_LIGHT_TIMER_PORT);
			break;
	}
}

static _Bool isSoilMoistureCapableForWatering(ZoneSettings *zoneSettings) {
	if (!isSoilSensorEnabled(zoneSettings -> zoneNumber)) {
		return true;
	}
	return (readZoneSoilMoisureInPercentages(zoneSettings -> zoneNumber)) <= (zoneSettings -> soilData.humidityPercentage);
}