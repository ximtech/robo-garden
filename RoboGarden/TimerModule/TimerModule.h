#pragma once

#include "../SettingsModule/ZoneSettings/ZoneCommonSettings.h"
#include "../SensorModule/SoilModule.h"

#define ZONE_TIMER_OUTPUT_DDR   DDRC
#define ZONE_TIMER_OUTPUT_PORT  PORTC

#define ZONE_1_WATER_TIMER_DDR DDC0
#define ZONE_1_LIGHT_TIMER_DDR DDC1
#define ZONE_2_WATER_TIMER_DDR DDC2
#define ZONE_2_LIGHT_TIMER_DDR DDC3
#define ZONE_3_WATER_TIMER_DDR DDC4
#define ZONE_3_LIGHT_TIMER_DDR DDC5
#define ZONE_4_WATER_TIMER_DDR DDC6
#define ZONE_4_LIGHT_TIMER_DDR DDC7

#define ZONE_1_WATER_TIMER_PORT PC0
#define ZONE_1_LIGHT_TIMER_PORT PC1
#define ZONE_2_WATER_TIMER_PORT PC2
#define ZONE_2_LIGHT_TIMER_PORT PC3
#define ZONE_3_WATER_TIMER_PORT PC4
#define ZONE_3_LIGHT_TIMER_PORT PC5
#define ZONE_4_WATER_TIMER_PORT PC6
#define ZONE_4_LIGHT_TIMER_PORT PC7

#define TIMER_EVENT_LIST_SIZE 24

typedef struct TimerEvent {
	_Bool isEventOccured;
	ZoneNumber zoneNumber;
	TimerType timerType;
	uint32_t totalSecondsDuration;
	uint32_t secondsPassed;
} TimerEvent;


void initEventHandler();
_Bool isNewEventOccurred();
volatile TimerEvent * getTimerEventList();
volatile LocalDateTime * getVolatileLocalDateTime();
_Bool isZoneTimersEnabled(Timer *startTimer, Timer *endTimer);
_Bool isTimeEventOccured(Timer *startTimer, Timer *endTimer);
_Bool isWeekDayTimerEventOccured(_Bool *weekDays);
