#include "StatusView.h"

#define ONE_SECOND_DELAY 31		// cycles count in 1 second with timer interrupt and 1024 prescaler
#define DELAY_IN_SECONDS_MACRO(seconds) (ONE_SECOND_DELAY * seconds)
#define MAX_SECONDS_FOR_OVERAL_VIEW 145

static volatile uint16_t viewSecondsDelay = 0;
static volatile uint16_t statusChangeTimer = 20;
static volatile uint8_t secondsForView = 0;

static void showZoneDHTData(ZoneNumber zoneNumber);
static char * getZoneNameShort(ZoneNumber zoneNumber);


void updateMainScreenStatus() {
	if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(1)) {
		goToXYLCD(0, 0);
		printLCDString(getLabelString(L_WHITESPACE));
		printLCDString(getLabelString(L_STATUS_PATTERN));
		printLCDString(getLabelString(L_WHITESPACE));
		printLCDString(getLabelString(L_STATUS_ONLINE));	// TODO: remove hardcoded value when network support will be available
		printLCDString(getLabelString(L_WHITESPACE));
	}
	else if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(5)) {
		goToXYLCD(0, 0);
		printLCDString(getLabelString(L_STATUS_ZONE_1));
		if (zone1Settings.isEnabled) {
			printLCDString(getLabelString(L_ON));
			printLCDString(getLabelString(L_EMPTY_LINE));
		} else {
			printLCDString(getLabelString(L_OFF));
			printLCDString(getLabelString(L_WHITESPACE));
		}
	}
	else if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(10)) {
		goToXYLCD(0, 0);
		printLCDString(getLabelString(L_STATUS_ZONE_2));
		if (zone2Settings.isEnabled) {
			printLCDString(getLabelString(L_ON));
			printLCDString(getLabelString(L_EMPTY_LINE));
		} else {
			printLCDString(getLabelString(L_OFF));
			printLCDString(getLabelString(L_WHITESPACE));
		}
	}
	else if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(15)) {
		goToXYLCD(0, 0);
		printLCDString(getLabelString(L_STATUS_ZONE_3));
		if (zone3Settings.isEnabled) {
			printLCDString(getLabelString(L_ON));
			printLCDString(getLabelString(L_EMPTY_LINE));
		} else {
			printLCDString(getLabelString(L_OFF));
			printLCDString(getLabelString(L_WHITESPACE));
		}
	}
	else if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(20)) {
		goToXYLCD(0, 0);
		printLCDString(getLabelString(L_STATUS_ZONE_4));
		if (zone4Settings.isEnabled) {
			printLCDString(getLabelString(L_ON));
			printLCDString(getLabelString(L_EMPTY_LINE));
		} else {
			printLCDString(getLabelString(L_OFF));
			printLCDString(getLabelString(L_WHITESPACE));
		}
	}
	
	if (zone1Settings.isEnabled) {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(25)) {
			showZoneDHTData(ZONE_1);
		}
	} else {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(20)) {
			statusChangeTimer = DELAY_IN_SECONDS_MACRO(35);
		}
	}
	
	if (zone2Settings.isEnabled) {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(35)) {
			showZoneDHTData(ZONE_2);
		}
	} else {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(35)) {
			statusChangeTimer = DELAY_IN_SECONDS_MACRO(45);
		}
	}
	
	if (zone3Settings.isEnabled) {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(45)) {
			showZoneDHTData(ZONE_3);
		}
	} else {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(45)) {
			statusChangeTimer = DELAY_IN_SECONDS_MACRO(55);
		}
	}
	
	if (zone4Settings.isEnabled) {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(55)) {
			showZoneDHTData(ZONE_4);
		}
	} else {
		if (statusChangeTimer == DELAY_IN_SECONDS_MACRO(55)) {
			statusChangeTimer = DELAY_IN_SECONDS_MACRO(65);
		}
	}
	
	if (statusChangeTimer > DELAY_IN_SECONDS_MACRO(65)) {
		statusChangeTimer = 0;
	}
	
	statusChangeTimer++;
}

void setStatusChangeTimer(uint16_t value) {
	statusChangeTimer = value;
}

static void showZoneDHTData(ZoneNumber zoneNumber) {
	volatile ZoneDHTData *zData = getZoneDHTData(zoneNumber);
	
	if (zData -> sensorStatus == DHT_SENSOR_OK) {
		goToXYLCD(0, 0);
		printLCDString(getZoneNameShort(zoneNumber));
		printfLCD(getLabelString(L_DHT_SHOW_DATA_PATTERN), zData -> integralT, DEGREE_SYMBOL, zData -> integralRH);
		
	} else if (zData -> sensorStatus == DHT_SENSOR_NOT_CONNECTED) {
		goToXYLCD(0, 0);
		printLCDString(getZoneNameShort(zoneNumber));
		printLCDString(getLabelString(L_SEMICOLON));
		printLCDStringAtPosition(0, 3, getLabelString(L_DHT_NOT_CONNECTED));
		
	} else {
		goToXYLCD(0, 0);
		printLCDString(getZoneNameShort(zoneNumber));
		printLCDString(getLabelString(L_SEMICOLON));
		printLCDStringAtPosition(0, 3, getLabelString(L_DHT_BROKEN_ERROR));
	}
}

static char * getZoneNameShort(ZoneNumber zoneNumber) {
	switch(zoneNumber) {
		case ZONE_1: 
			return getLabelString(L_ZONE_1_SHORT);
		case ZONE_2: 
			return getLabelString(L_ZONE_2_SHORT);
		case ZONE_3: 
			return getLabelString(L_ZONE_3_SHORT);
		case ZONE_4: 
			return getLabelString(L_ZONE_4_SHORT);
		default: 
			return getLabelString(L_WHITESPACE);
	}
}
