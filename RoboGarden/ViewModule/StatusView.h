#pragma once

#include "../SettingsModule/ZoneSettings/ZoneCommonSettings.h"
#include "../TimerModule/TimerModule.h"
#include "../SensorModule/DHTModule.h"

void updateMainScreenStatus();
void setStatusChangeTimer(uint16_t value);