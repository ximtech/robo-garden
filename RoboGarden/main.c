#include "projectSettings.h"

#include "MenuModule/MenuModule.h"
#include "MemoryModule/EEPROM.h"
#include "TimerModule/TimerModule.h"
#include "ViewModule/StatusView.h"
#include "SensorModule/SoilModule.h"

#define START_MESSAGE_DELAY 2000
#define MAX_LOAD_BAR_LENGTH_SINGLE_LINE (LCD_COL_COUNT - 2)	// position at the end line and after progress bar arrow pointer 
#define MAX_LOAD_BAR_LENGTH_MULTI_LINE 5
#define LCD_PROGRESS_BAR_START_COLUMN_AT_MULTI_LINE_VIEW 10
#define PROGRESS_BAR_UPDATE_DELAY 500

static void displayEmptyProgressBar(uint8_t col, uint8_t row);
static void printProgressBarElementAtPosition(uint8_t col, uint8_t row);
static void clearProgressBarElementAtPosition(uint8_t col, uint8_t row);
static inline uint32_t convertSecondsToProgressBarValue(uint8_t progresBarLength, uint32_t totalSeconds, uint32_t secondsPassed);
static char * getTimerTypeStringValueShort(TimerType timerType);


ISR(TIMER2_OVF_vect) {
	
	switch(settingsItem) {
		case MAIN_SCREEN_STATUS_UPDATE:
			updateMainScreenStatus();
			break;
		case SET_DATE_TIME:
			processButtonsForDateTimeSettings();
			break;
		case ZONE_SETTINGS_ON_OFF:
			processButtonsForZoneOnOffSettings();
			break;
		case ZONE_SETTINGS_TIME:
			processButtonsForZoneTimeSettings();
			break;
		case ZONE_SETTINGS_WEEK_DAY:
			processButtonsForZoneWeekDaySettings();
			break;
		case ZONE_SETTINGS_TEMPERATURE:
			processButtonsForZoneTemperatureSettings();
			break;
		case ZONE_SETTINGS_VIEW_DHT:
			processBackButtonForZoneDHTView();
			break;
		case ZONE_SETTINGS_SOIL:
			processButtonsForZoneSoilSettings();
			break;
	}
	
	resetTimer2();
}


static void displayHomeScreen() {	// main screen view
	clearLCD();
	setStatusChangeTimer(0);
	volatile LocalDateTime *localDateTime;
	settingsItem = MAIN_SCREEN_STATUS_UPDATE;
	enableInterruptTimer2();
	
	while (bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN)) {
		
		if (isNewEventOccurred()) {	// if timer event occurred, then display timer progress bar status. if more then one event occurred then display in list view
			disableInterruptTimer2();
			clearLCD();
			uint8_t eventIndexList[TIMER_EVENT_LIST_SIZE];
			uint8_t timerEventCounter;
			volatile TimerEvent *timerEventList = getTimerEventList();
			volatile TimerEvent timerEventItem;
			uint8_t secondsToProgressBarValue;
			uint8_t barPositionValues[LCD_ROW_COUNT] = { [0 ... (LCD_ROW_COUNT - 1)] = 0 };
			
			do {
				
				timerEventCounter = 0;
				for (uint8_t i = 0; i < TIMER_EVENT_LIST_SIZE; i++) {	// count active events in list
					if (timerEventList[i].isEventOccured) {
						eventIndexList[timerEventCounter] = i;
						timerEventCounter++;
					}
				}
				
				if (timerEventCounter == 1) {	// View single event with progress bar
					timerEventItem = timerEventList[eventIndexList[timerEventCounter - 1]];
					goToXYLCD(0, 0);
					printfLCD(getLabelString(L_TIMER_EVENT_SINGL_PATTERN), timerEventItem.zoneNumber);
					printLCDString(getTimerTypeStringValueShort(timerEventItem.timerType));
					printLCDString(getLabelString(L_EMPTY_LINE));
					
					secondsToProgressBarValue = convertSecondsToProgressBarValue(MAX_LOAD_BAR_LENGTH_SINGLE_LINE, timerEventItem.totalSecondsDuration, timerEventItem.secondsPassed);
					displayEmptyProgressBar(1, 0);
					
					for (uint8_t i = 1; i <= secondsToProgressBarValue; i++) {	// start from 1, because on 0 is progress bar pointer element
						printProgressBarElementAtPosition(1, i);
					}
					
					for (uint8_t i = MAX_LOAD_BAR_LENGTH_SINGLE_LINE; i > secondsToProgressBarValue; i--) {	// go from the progress bar end and clean not filled bars(preventing progress bar overlapping view) 
						clearProgressBarElementAtPosition(1, i);
					}
					_delay_ms(PROGRESS_BAR_UPDATE_DELAY);
					
				} else if (timerEventCounter > 1) {	// View multiple events with progress bar
					for (uint8_t i = 0; i < LCD_ROW_COUNT; i++) {
						timerEventItem = timerEventList[eventIndexList[timerEventCounter - 1 - i]];
						goToXYLCD(i, 0);
						printfLCD(getLabelString(L_TIMER_EVENT_PATTERN), timerEventItem.zoneNumber, getTimerTypeStringValueShort(timerEventItem.timerType));
						printLCDString(getTimerTypeStringValueShort(timerEventItem.timerType));
						
						secondsToProgressBarValue = convertSecondsToProgressBarValue(MAX_LOAD_BAR_LENGTH_MULTI_LINE, timerEventItem.totalSecondsDuration, timerEventItem.secondsPassed);
						displayEmptyProgressBar(i, (LCD_PROGRESS_BAR_START_COLUMN_AT_MULTI_LINE_VIEW - 1));	// Display progress bar at single line for each timer event, first place arrow pointer symbol
				
						for (uint8_t j = barPositionValues[i]; j < secondsToProgressBarValue; j++) {	// print every progress bar element only once at calculated and saved value
							printProgressBarElementAtPosition(i, (j + LCD_PROGRESS_BAR_START_COLUMN_AT_MULTI_LINE_VIEW));
						}
						
						for (uint8_t j = MAX_LOAD_BAR_LENGTH_SINGLE_LINE; j >= (secondsToProgressBarValue + LCD_PROGRESS_BAR_START_COLUMN_AT_MULTI_LINE_VIEW); j--) {
							clearProgressBarElementAtPosition(i, j);
						}
						
						barPositionValues[i] = secondsToProgressBarValue;	// save previous progress bar element placement value to not redraw entire load bar in next iteration
					}
					_delay_ms(PROGRESS_BAR_UPDATE_DELAY);
				}
				
			} while (timerEventCounter != 0 && bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN));
			
			if (bit_is_clear(BUTTON_PIN, BUTTON_ENTER_PIN)) {
				break;
			}
			
			clearLCD();
			setStatusChangeTimer(0);
			enableInterruptTimer2();
		}
		
		localDateTime = getVolatileLocalDateTime();
		goToXYLCD(1, 0);
		printfLCD(getLabelString(L_DATE_PATTERN),
			localDateTime -> hour,
			localDateTime -> minute,
			localDateTime -> second,
			localDateTime -> day,
			localDateTime -> month);
	}
	
	disableInterruptTimer2();
	while(true) {
		displayMenu();
	}
}

void initZoneSettings() {		// Get data from eeprom or set default settings to zones
	loadZoneSettingsFromEEPROM(ZONE_1, &zone1Settings);
	loadZoneSettingsFromEEPROM(ZONE_2, &zone2Settings);
	loadZoneSettingsFromEEPROM(ZONE_3, &zone3Settings);
	loadZoneSettingsFromEEPROM(ZONE_4, &zone4Settings);
}

int main(void) {
	initLCD();
	printLCDString(getLabelString(L_PROJECT_NAME));
	goToXYLCD(1, 0);
	printfLCD(getLabelString(L_VERSION_PATERN), PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR);
	_delay_ms(START_MESSAGE_DELAY);
	
	static uint8_t loadBarStartElement[] = { 0b10000, 0b11000, 0b11100, 0b11110, 0b11110, 0b11100, 0b11000, 0b10000 };
	static uint8_t loadBarEndElement[] = { 0b00001, 0b00011, 0b00111, 0b01111, 0b01111, 0b00111, 0b00011, 0b00001 };
	static uint8_t settingsPointer[] = { 0b00000, 0b00000, 0b00100, 0b01000, 0b11111, 0b01000, 0b00100, 0b00000 };
	static uint8_t loadBarProgressElement[] = { 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111 };
	
	createCustomCharacter(POINTER_TO_RIGHT_SYMBOL_NUM, loadBarStartElement);
	createCustomCharacter(POINTER_TO_LEFT_SYMBOL_NUM, loadBarEndElement);
	createCustomCharacter(ARROW_POINTER_SYMBOL_NUM, settingsPointer);
	createCustomCharacter(LOAD_BAR_ELEMENT_SYMBOL_NUM, loadBarProgressElement);
	
	initMenu(displayHomeScreen);
	initRTC();
	
	// First RTC start can contain invalid date time values. So the clock can't be started, need set initial time
	LocalDateTime *localDateTime = getRTCdateTime();
	_Bool isRTCDateTimeInvalid = (localDateTime -> second > (SECONDS_IN_MINUTE - 1)) || (localDateTime -> day == 0) || (localDateTime -> month == 0);
	if (isRTCDateTimeInvalid) {	
		setRTCtime(0, 0, 0);
		setRTCdate(FIRST_DAY_IN_MONTH, JANUARY, MIN_YEAR);
	}
	
	initZoneSettings();
	initTimer2(TIMER2_PRESCALER_1024, 0);
	initEventHandler();
	initDHTHandler();
	initSoilSensorHandler();
	
	displayHomeScreen();
}

static void displayEmptyProgressBar(uint8_t col, uint8_t row) {
	goToXYLCD(col, row);
	printLCDCustomCharacter(POINTER_TO_RIGHT_SYMBOL_NUM);	// Display progress bar start
	goToXYLCD(col, LCD_LAST_COLUMN_INDEX);
	printLCDCustomCharacter(POINTER_TO_LEFT_SYMBOL_NUM);	// Display progress bar end
}

static void printProgressBarElementAtPosition(uint8_t col, uint8_t row) {
	goToXYLCD(col, row);
	printLCDCustomCharacter(LOAD_BAR_ELEMENT_SYMBOL_NUM);
}

static void clearProgressBarElementAtPosition(uint8_t col, uint8_t row) {
	goToXYLCD(col, row);
	printLCDString(getLabelString(L_WHITESPACE));
}

static inline uint32_t convertSecondsToProgressBarValue(uint8_t progresBarLength, uint32_t totalSeconds, uint32_t secondsPassed) {
	return (progresBarLength * secondsPassed) / totalSeconds;
}

static char * getTimerTypeStringValueShort(TimerType timerType) {	// TODO: refactor, move to progmem
	if (timerType == TIMER1_WATERING) {
		return getLabelString(L_TIMER_TYPE_WATERING_1_SHORT);
	} else if (timerType == TIMER2_WATERING) {
		return getLabelString(L_TIMER_TYPE_WATERING_2_SHORT);
	}  else if (timerType == TIMER3_WATERING) {
		return getLabelString(L_TIMER_TYPE_WATERING_3_SHORT);
	} else if (timerType == TIMER1_LIGHT) {
		return getLabelString(L_TIMER_TYPE_LIGHT_1_SHORT);
	} else if (timerType == TIMER2_LIGHT) {
		return getLabelString(L_TIMER_TYPE_LIGHT_2_SHORT);
	} else if (timerType == TIMER3_LIGHT) {
		return getLabelString(L_TIMER_TYPE_LIGHT_3_SHORT);
	}
	return getLabelString(L_WHITESPACE);
}