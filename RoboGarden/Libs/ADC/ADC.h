#pragma once

#include "../../projectSettings.h"

#include <avr/io.h>
#include <stdint.h>

#ifndef F_CPU
#error "F_CPU must be defined in ADC.h"
#endif

#define ADC_DDR DDRF	// define ADC port direction

#define ADC_PRESCALE_2	  (1 << ADPS0)
#define ADC_PRESCALE_4    (1 << ADPS1
#define ADC_PRESCALE_8   ((1 << ADPS0) | (1 << ADPS1))
#define ADC_PRESCALE_16   (1 << ADPS2)
#define ADC_PRESCALE_32  ((1 << ADPS0) | ( 1 << ADPS2))
#define ADC_PRESCALE_64  ((1 << ADPS2) | ( 1 << ADPS1))
#define ADC_PRESCALE_128 ((1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2))


#define ADC_MAX_CLK 200000	// The maximum ADC clock on this device line

#if ((F_CPU / 2) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_2

#elif ((F_CPU / 4) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_4

#elif ((F_CPU / 8) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_8

#elif ((F_CPU / 16) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_16

#elif ((F_CPU / 32) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_32

#elif ((F_CPU / 64) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_64

#elif ((F_CPU / 128) <= ADC_MAX_CLK)
#define ADC_PRESCALE ADC_PRESCALE_128

#endif


void initADC();
uint16_t getADC10BitValue(uint8_t channel);
uint8_t getADC8BitValue(uint8_t channel);