#pragma once

#include "../../projectSettings.h"

#include "RTC_I2C.h"
#include "../I2C/I2C_Master.h"
#include <avr/io.h>

#ifndef F_CPU
#error "F_CPU is not defined in RTC_I2C.h"
#endif

#define RTC_WRITE_ADDRESS	0xD0	// RTC DS1307 slave write address
#define RTC_READ_ADDRESS	0xD1	// make LSB bit high of slave address for read 

#define RTC_TIME_START_ADDRESS 0
#define RTC_DATE_START_ADDRESS 3

#define RTC_SECONDS_ADDRESS  0
#define RTC_MINUTES_ADDRESS  1
#define RTC_HOURS_ADDRESS	 2
#define RTC_WEEK_DAY_ADDRESS 3
#define RTC_DAY_ADDRESS		 4
#define RTC_MONTH_ADDRESS	 5
#define RTC_YEAR_ADDRESS	 6

#define RTC_HOUR_FORMAT_BIT 6
#define RTC_AM_PM_BIT		5

#define HOURS_IN_24H_FORMAT 24
#define HOURS_IN_12H_FORMAT 12
#define MINUTES_IN_HOUR		60
#define SECONDS_IN_MINUTE	60
#define SECONDS_IN_HOUR     3600
#define MIN_YEAR			2000
#define MAX_YEAR			2099
#define FIRST_DAY_IN_MONTH  1
#define DAYS_IN_WEEK		7

typedef enum HourFormat {
	HOUR_FORMAT_12,
	HOUR_FORMAT_24	
} HourFormat;

typedef enum AmPm {
	AM, PM
} AmPm;

typedef enum WeekDay {
	SUNDAY    = 1,
	MONDAY    = 2,
	TUESDAY	  = 3,
	WEDNESDAY = 4,
	THURSDAY  = 5,
	FRIDAY    = 6,
	SATURDAY  = 7
} WeekDay;

typedef enum Month {
	JANUARY   = 1,
	FEBRUARY  = 2,
	MARCH	  = 3,
	APRIL	  = 4,
	MAY		  = 5,
	JUNE	  = 6,
	JULY	  = 7,
	AUGUST	  = 8,
	SEPTEMBER = 9,
	OCTOBER   = 10,
	NOVEMBER  = 11,
	DECEMBER  = 12
} Month;

typedef struct LocalTime {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	HourFormat hourFormat;
	AmPm amPm;
} LocalTime;

typedef struct LocalDate {
	uint8_t day;
	Month month;
	uint16_t year;
	WeekDay weekDay;
} LocalDate;

typedef struct LocalDateTime {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t day;
	Month month;
	uint16_t year;
	WeekDay weekDay;
	HourFormat hourFormat;
	AmPm amPm;
} LocalDateTime;

inline char * getWeekDayNameShort(WeekDay weekDay) {
	char * const WEEK_DAY_NAME_SHORT[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
	return WEEK_DAY_NAME_SHORT[weekDay - 1];
}


inline WeekDay getDayOfWeek(uint8_t day, uint8_t month, uint16_t year) {			// find day of a given date
	uint8_t t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };					// predefined time constants
	year -= (month < 3);
	return (( year + (year / 4) - (year / 100) + (year / 400) + t[month - 1] + day ) % 7) + 1;
}

inline uint8_t getDaysInMonth(uint8_t month, uint16_t year) {
	if (year < 1528 || month > 12 || month < 1) {	// before this year the Gregorian Calendar was not defined
		return 0;
	} else if (month == 4 || month == 6 || month == 9 || month == 11) {
		return 30;
	} else if (month == 2) {
		return (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) ? 29 : 28);
	} else {
		return 31;
	}
}

void initRTC();		// by default set 24h format

void setRTCtime(uint8_t hours, uint8_t minutes, uint8_t seconds);
void setRTCtime12hFormat(uint8_t hours, uint8_t minutes, uint8_t seconds, AmPm amPm);
void setRTCdate(uint8_t day, Month month, uint16_t year);

void setRTChoursIn24hFormat(uint8_t hours);
void setRTChoursIn12hFormat(uint8_t hours, AmPm amPm);
void setRTCminutes(uint8_t minutes);
void setRTCday(uint8_t day);
void setRTCmonth(uint8_t month);
void setRTCyear(uint16_t year);
void setRTCweekDay(WeekDay weekDay);
void setWeekDayByDate(uint8_t day, uint8_t month, uint16_t year);

uint32_t getSeconds(uint8_t hour, uint8_t minute, uint8_t second);

LocalTime * getRTCtime();
LocalDate * getRTCdate();
LocalDateTime * getRTCdateTime();