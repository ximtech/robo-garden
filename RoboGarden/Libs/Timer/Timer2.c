#include "Timer2.h"
#include <avr/interrupt.h>


void initTimer2(uint8_t prescaler, uint8_t initialValue) {
	resetTimer2();
	setTimer2(initialValue);
	TCCR2 &= ~((1 << WGM21) | (1 << WGM20));		// set normal mode
	TCCR2 = prescaler & 0x07;        //set the prescaler and mask bits that aren't the prescaler
}

uint8_t getTimer2Value() {
	return TCNT2;                     //return the timer counter 0 register value
}

void setTimer2(uint8_t value) {
	TCNT2 = value;                       //set the timer0 register
}

void resetTimer2() {
	TCNT2 = 0;                       //set the timer0 register to 0, clearing the timer
	TIFR = 0x1;
}

void enableInterruptTimer2() {
	TIMSK |= (1 << TOV2);            //set the timer overflow interrupt enable 0 bit
	sei();                            //set the global interrupt flag
}

void disableInterruptTimer2() {
	TIMSK &= ~(1 << TOIE2);           //clear the timer overflow interrupt enable 0 bit
}