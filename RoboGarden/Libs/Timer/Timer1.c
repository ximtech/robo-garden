#include "Timer1.h"
#include <avr/interrupt.h>

void initTimer1_16bit(uint8_t prescaler) {
	
	resetTimer1_16bit();
	TCCR1A &= ~((1 << WGM13) | (1 << WGM12) | (1 << WGM11) | (1 << WGM10));		// set normal mode
	TCCR1A = prescaler & 0x07;        //set the prescaler and mask bits that aren't the prescaler
}

void resetTimer1_16bit() {
	TCNT1H = 0;                       //set the timer1_16bit register to 0, clearing the timer
	TCNT1L = 0;
	TIFR = 0x1;
}

void enableInterruptTimer1_16bit() {
	TIMSK |= (1 << OCIE1A);            //set the timer overflow interrupt enable 0 bit
	sei();                            //set the global interrupt flag
}

void disableInterruptTimer1_16bit() {
	TIMSK &= ~(1 << OCIE1A);           //clear the timer overflow interrupt enable 0 bit
}