#pragma once

#include "TimerCommon.h"
#include <stdint.h>

void initTimer1_16bit(uint8_t prescaler);
void resetTimer1_16bit();
void enableInterruptTimer1_16bit();
void disableInterruptTimer1_16bit();