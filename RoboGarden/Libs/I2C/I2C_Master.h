#pragma once

#include "../../projectSettings.h"
#include <avr/io.h>
#include "I2C_Master.h"

/*
* Master I2C usage:
* For sending data:
*	1. initI2C(prescaler) - set value from i2cPrescaler enum
*	2. startI2C(device_address) - can validate response status, must be OK
*	3. writeI2C(0x00) - send initial data
*	4. Write your data using: writeI2C(data)
*	5. stopI2C()
*
* For receiving data:
*	1. initI2C(prescaler)
*	2. startI2C(device_address)
*	3. writeI2C(0x00) - send initial data
*	4. Read your data using: readI2C()
*	5. readNackI2C() - read flush data with nack
*	6. stopI2C()
*
* Note: If slave address, for example is 0xA0
* Then write address: 0xA0
* And read address: 0xA1 (increment one)
*/

//#define F_CPU 8000000UL

#ifndef F_CPU
#error "F_CPU is not defined in I2C_Master.h"
#endif

// I2C clock in Hz
#define SCL_CLOCK  100000L

enum i2cMasterStatus {
	I2C_OK, 
	I2C_START_ERROR,
	I2C_ACK_ERROR,
	I2C_NACK_RECEIVED,
	I2C_DATA_TX_ERROR
};

enum i2cPrescaler {
	I2C_PRESCALER_1,
	I2C_PRESCALER_4,
	I2C_PRESCALER_16,
	I2C_PRESCALER_64
};

void initMasterI2C(enum i2cPrescaler prescaler);

enum i2cMasterStatus startMasterI2C(uint8_t address);
void stopMasterI2C();

enum i2cMasterStatus writeMasterI2C(char dataByte);
char readMasterI2C();
char readMasterNackI2C();
void stopMasterI2C();