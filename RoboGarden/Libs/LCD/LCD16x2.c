#include "LCD16x2.h"
#include <util/delay.h>
#include <stdarg.h>

static uint8_t dataPins[8];
static uint8_t displayControlParameters;
static char formatBuffer[LCD_COL_COUNT + 1];

uint8_t loadBarStartElement[8] = { 0b10000, 0b11000, 0b11100, 0b11110, 0b11110, 0b11100, 0b11000, 0b10000 };
uint8_t loadBarProgressElement[8] = { 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111 };
uint8_t loadBarEndElement[8] = { 0b00001, 0b00011, 0b00111, 0b01111, 0b01111, 0b00111, 0b00011, 0b00001 };
	
uint8_t barRowLocation = 0;
uint8_t barColLocation = 0;
uint8_t barTotalLength = 0;
uint8_t barProgress = 0;

#ifdef USE_8_BIT_MODE
void initLCDinEightBitMode() {
	
	// Configure pins as output
	LCD_COMMAND_DDR = LCD_COMMAND_DDR
	| (1 << LCD_RS)
	| (1 << LCD_RW)
	| (1 << LCD_EN);
	
	LCD_DATA_DDR = LCD_DATA_DDR
	| (1 << LCD_D0)
	| (1 << LCD_D1)
	| (1 << LCD_D2)
	| (1 << LCD_D3)
	| (1 << LCD_D4)
	| (1 << LCD_D5)
	| (1 << LCD_D6)
	| (1 << LCD_D7);
	
	_delay_ms(20);		// LCD Power ON delay always >15ms
	
	LCD_COMMAND_PORT = LCD_COMMAND_PORT
	& ~(1 << LCD_EN)
	& ~(1 << LCD_RS)
	& ~(1 << LCD_RW);
	
	LCD_DATA_PORT = LCD_DATA_PORT
	& ~(1 << LCD_D0)
	& ~(1 << LCD_D1)
	& ~(1 << LCD_D2)
	& ~(1 << LCD_D3)
	& ~(1 << LCD_D4)
	& ~(1 << LCD_D5)
	& ~(1 << LCD_D6)
	& ~(1 << LCD_D7);
	
	dataPins[0] = LCD_D0;
	dataPins[1] = LCD_D1;
	dataPins[2] = LCD_D2;
	dataPins[3] = LCD_D3;
	dataPins[4] = LCD_D4;
	dataPins[5] = LCD_D5;
	dataPins[6] = LCD_D6;
	dataPins[7] = LCD_D7;
	
	commandLCD(LCD_TWO_LINES_5X8_MATRIX_8BIT_MODE);
}

void pulseEnable() {
	LCD_COMMAND_PORT |= (1 << LCD_EN);		// Enable pulse
	_delay_us(1);
	LCD_COMMAND_PORT &= ~(1 << LCD_EN);
	_delay_ms(2);
}

void writeToPort(uint8_t port, uint8_t value) {
	if (value) {
		LCD_DATA_PORT |= (1 << port);
	} else {
		LCD_DATA_PORT &= ~(1 << port);
	}
}

void writeEightBits(uint8_t value) {
	for (uint8_t i = 0; i < 8; i++) {
		writeToPort(dataPins[i], (value >> i) & 0x01);
	}
}

void commandLCD(uint8_t command) {
	writeEightBits(command);
	LCD_COMMAND_PORT &= ~(1 << LCD_RS);		// RS=0 command reg.
	LCD_COMMAND_PORT &= ~(1 << LCD_RW);		// RW=0 Write operation
	pulseEnable();
}

void printLCDChar(unsigned char charData) {
	writeEightBits(charData);
	LCD_COMMAND_PORT |= (1 << LCD_RS);		// RS=1 data reg.
	LCD_COMMAND_PORT &= ~(1 << LCD_RW);		// RW=0 Write operation
	pulseEnable();
}
#endif	//USE_8_BIT_MODE

//-----------------------------------------------------------------------------

#ifdef USE_4_BIT_MODE
void initLCDinFourBitMode() {
	
	// Configure pins as output
	LCD_DDR = LCD_DDR	
	| (1 << LCD_RS)
	| (1 << LCD_RW)
	| (1 << LCD_EN)
	| (1 << LCD_D4)
	| (1 << LCD_D5)
	| (1 << LCD_D6)
	| (1 << LCD_D7);
		 
	_delay_ms(20);		// LCD Power ON delay always >15ms
		 
	LCD_PORT = LCD_PORT
	& ~(1 << LCD_EN)
	& ~(1 << LCD_RS)
	& ~(1 << LCD_RW);
		 
	dataPins[0] = LCD_D4;
	dataPins[1] = LCD_D5;
	dataPins[2] = LCD_D6;
	dataPins[3] = LCD_D7;
		 
	commandLCD(0x33);
	commandLCD(0x32);	// Send for 4 bit initialization of LCD
	commandLCD(LCD_TWO_LINES_5X8_MATRIX_4BIT_MODE);
}

void writeToPort(uint8_t port, uint8_t value) {
	if (value) {
		LCD_PORT |= (1 << port);
	} else {
		LCD_PORT &= ~(1 << port);
	}
}

void writeFourBits(uint8_t nibble) {
	for (uint8_t i = 0; i < 4; i++) {
		writeToPort(dataPins[i], (nibble >> i) & 0x01);
	}
}

void pulseEnableUpper() {
	LCD_PORT |= (1 << LCD_EN);	// Enable pulse
	_delay_us(1);
	LCD_PORT &= ~(1 << LCD_EN);
	_delay_us(200);
}

void pulseEnableLower() {
	LCD_PORT |= (1 << LCD_EN);
	_delay_us(1);
	LCD_PORT &= ~(1 << LCD_EN);
	_delay_ms(2);
}

void commandLCD(uint8_t command) {
	writeFourBits(command >> 4);	// sending upper nibble

	LCD_PORT &= ~(1 << LCD_RS);	// RS=0, command reg.
	LCD_PORT &= ~(1 << LCD_RW);
	pulseEnableUpper();
		
	writeFourBits(command);	// sending lower nibble
	pulseEnableLower();
}

void printLCDChar(unsigned char charData) {
	writeFourBits(charData >> 4);	// sending upper nibble
		
	LCD_PORT |= (1 << LCD_RS);	// RS=1, data reg.
	LCD_PORT &= ~(1 << LCD_RW);
	pulseEnableUpper();
		
	writeFourBits(charData);
	pulseEnableLower();
}
#endif	//USE_4_BIT_MODE


void initLCD() {
	#ifdef USE_8_BIT_MODE			// Configure lcd for 8 bit mode
		initLCDinEightBitMode();
	#elif defined(USE_4_BIT_MODE)	// Configure lcd for 4 bit mode
		initLCDinFourBitMode();
	#endif
	
	commandLCD(LCD_DISPLAY_ON_CURSOR_OFF);
	commandLCD(LCD_SHIFT_CURSOR_RIGHT);		// Auto Increment cursor from left to right
	clearLCD();
}

void clearLCD() {
	commandLCD(LCD_CLEAR_DISPLAY);
	_delay_ms(2);		// Clear display command delay > 1.63 ms
	returnHomeLCD();
}

void turnOnLCD() {
	displayControlParameters |= LCD_DISPLAYON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void turnOffLCD() {
	displayControlParameters &= ~LCD_DISPLAYON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void returnHomeLCD() {
	commandLCD(LCD_MOVE_CURSOR_AT_FIRST_LINE_BEGINNING);
}

void enableBlinkingCursorLCD() {
	displayControlParameters |= LCD_DISPLAY_ON_CURSOR_BLINKING;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void disableBlinkingCursorLCD() {
	commandLCD(LCD_DISPLAY_ON_CURSOR_OFF);
}

void enableCursorLCD() {
	displayControlParameters |= LCD_CURSORON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void disableCursorLCD() {
	displayControlParameters &= ~LCD_CURSORON;
	commandLCD(LCD_DISPLAYCONTROL | displayControlParameters);
}

void moveCursorLeftLCD() {
	commandLCD(LCD_SHIFT_POSITION_LEFT);
}

void moveCursorRightLCD() {
	commandLCD(LCD_SHIFT_POSITION_LEFT);
}

void moveCursorAtSecondLineBegginingLCD() {
	commandLCD(LCD_MOVE_CURSOR_AT_SECOND_LINE_BEGINNING);
}

void moveDisplayLeftLCD() {
	commandLCD(LCD_SHIFT_DISPLAY_LEFT);
}

void moveDisplayRightLCD() {
	commandLCD(LCD_SHIFT_DISPLAY_RIGHT);
}

void setCursotIncrementFromLeftToRightLCD() {
	displayControlParameters |= LCD_ENTRYLEFT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void setCursotIncrementFromRightToLeftLCD() {
	displayControlParameters &= ~LCD_ENTRYLEFT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void enableAutoScrollLCD() {
	displayControlParameters |= LCD_ENTRYSHIFTINCREMENT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void disableAutoScrollLCD() {
	displayControlParameters &= ~LCD_ENTRYSHIFTINCREMENT;
	commandLCD(LCD_ENTRYMODESET | displayControlParameters);
}

void goToXYLCD(uint8_t row, uint8_t pos) {
	if (row == 0 && pos < LCD_COL_COUNT) {
		commandLCD((pos & 0x0F) | LCD_MOVE_CURSOR_AT_FIRST_LINE_BEGINNING);				// Command of first row and required position < 16
	} else if (row == 1 && pos < LCD_COL_COUNT) {
		commandLCD((pos & 0x0F) | LCD_MOVE_CURSOR_AT_SECOND_LINE_BEGINNING);			// Command of second row and required position < 16
	}
}

void printLCDString(char *string) {
	for (uint8_t i = 0; string[i] != 0; i++) {
		printLCDChar(string[i]);
	}
}

void printLCDStringAtPosition(uint8_t row, uint8_t pos, char *str) {			// Send string to LCD with xy position
	goToXYLCD(row, pos);
	printLCDString(str);
}

void createCustomCharacter(uint8_t location, uint8_t *charmap) {	// location from 0 to 7
	if (location < 8) {
		commandLCD(LCD_SETCGRAMADDR + (location * 8));	// Command 0x40 and onwards forces the device to point CGRAM address
		for (uint8_t i = 0; i < 8; i++) {
			printLCDChar(charmap[i]);
		}
		returnHomeLCD();
	}
}

void printLCDCustomCharacter(uint8_t location) {	// location from 0 to 7
	if (location < 8) {
		printLCDChar(location);
	}
}

void printfLCD(char *format, ...) {
	va_list args;

	va_start(args, format);
	vsnprintf(formatBuffer, LCD_COL_COUNT + 1, format, args);
	va_end(args);

	printLCDString(formatBuffer);
}

void initProgressBar(uint8_t len, uint8_t row, uint8_t col) {
	_Bool isBarLengthValid = (len <= LCD_COL_COUNT - 1) && ((len - 2) >= 0);
	_Bool isRowValid = (row == 0) || (row == 1);
	
	if (isBarLengthValid && isRowValid) {
		
		createCustomCharacter(0, loadBarStartElement);
		createCustomCharacter(1, loadBarProgressElement);
		createCustomCharacter(2, loadBarEndElement);
		
		barRowLocation = row;
		barColLocation = col + 1;
		barTotalLength = len - 1;
		
		goToXYLCD(row, col);
		printLCDCustomCharacter(0);
		
		goToXYLCD(row, (col + len));
		printLCDCustomCharacter(2);
		
	}
}

void incrementProgressBar() {
	if (barTotalLength != 0 && barProgress < barTotalLength) {
		goToXYLCD(barRowLocation, barColLocation++);
		printLCDCustomCharacter(1);
	}
	barProgress++;
}

