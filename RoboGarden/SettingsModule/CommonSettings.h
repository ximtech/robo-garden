#pragma once

#include "../projectSettings.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/sfr_defs.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/interrupt.h>

#include "../Libs/LCD/LCD16x2.h"
#include "../Libs/Menu/debounce.h"
#include "../Libs/Menu/menu.h"
#include "../Libs/RTC/RTC_I2C.h"
#include "../Libs/Timer/Timer2.h"

#define DIGIT_BLINK_DELAY 500
#define LCD_LAST_COLUMN_INDEX (LCD_COL_COUNT - 1)

#define POINTER_TO_RIGHT_SYMBOL_NUM 2
#define POINTER_TO_LEFT_SYMBOL_NUM  3
#define ARROW_POINTER_SYMBOL_NUM    4
#define LOAD_BAR_ELEMENT_SYMBOL_NUM 5

#define ERROR_MESSAGE_SHOW_TIMEOUT 2000

typedef enum DisplaySettings {
	MAIN_SCREEN_STATUS_UPDATE,
	SET_DATE_TIME,
	ZONE_SETTINGS_ON_OFF,
	ZONE_SETTINGS_TIME,
	ZONE_SETTINGS_WEEK_DAY,
	ZONE_SETTINGS_TEMPERATURE,
	ZONE_SETTINGS_VIEW_DHT,
	ZONE_SETTINGS_SOIL
} DisplaySettings;


DisplaySettings settingsItem;

inline void lockEnterButton() {		// lock enter for preventing multiple button press
	loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
}