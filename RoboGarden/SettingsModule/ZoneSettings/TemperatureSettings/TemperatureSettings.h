#pragma once

#include "../../../MemoryModule/EEPROM.h"

void processButtonsForZoneTemperatureSettings();

void setMaxTemperatureForZone1();
void setMinTemperatureForZone1();

void setMaxTemperatureForZone2();
void setMinTemperatureForZone2();

void setMaxTemperatureForZone3();
void setMinTemperatureForZone3();

void setMaxTemperatureForZone4();
void setMinTemperatureForZone4();
