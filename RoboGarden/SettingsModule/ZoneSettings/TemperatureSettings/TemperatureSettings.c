#include "TemperatureSettings.h"

#define MAX_CURSOR_POSITION 4
#define MAX_INTEGRAL_TEMPERATURE_TRESHOLD 40
#define MAX_DECIMAL_TEMPERATURE_TRESHOLD  9
#define MAX_INTEGRAL_HYSTERESIS_TRESHOLD  10
#define MAX_DECIMAL_HYSTERESIS_TRESHOLD   9

static volatile _Bool needEscapeToMenu;

static enum TemperatureSetting {
	MAX_TEMPERATURE_SETTINGS,
	MIN_TEMPERATURE_SETTINGS
} tempSettings;

static volatile enum CursorPosition {
	INTEGRAL_TEMPERATURE,
	DECIMAL_TEMPERATURE,
	INTEGRAL_HYSTERESIS,
	DECIMAL_HYSTERESIS
} cursorPosition;

static volatile Temperature temperature = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 };

static void setupMenuTemperature(Temperature *temp);
static void displayZoneTemperatureSettings();
static void setupZoneTemperatureSettings(ZoneSettings *zoneSettings);
static _Bool isTemperatureConfiguredCorrectly();


void processButtonsForZoneTemperatureSettings() {
		
		debounce();
		
		if (button_down(BUTTON_UP_MASK)) {
			switch(cursorPosition) {
				case INTEGRAL_TEMPERATURE:
					if (temperature.integralT < MAX_INTEGRAL_TEMPERATURE_TRESHOLD) {
						temperature.integralT++;
					} else {
						temperature.integralT = 0;
					}
					break;
				case DECIMAL_TEMPERATURE:
					if (temperature.decimalT < MAX_DECIMAL_TEMPERATURE_TRESHOLD) {
						temperature.decimalT++;
					} else {
						temperature.decimalT = 0;
					}
					break;
				case INTEGRAL_HYSTERESIS:
					if (temperature.integralHyst < MAX_INTEGRAL_HYSTERESIS_TRESHOLD) {
						temperature.integralHyst++;
					} else {
						temperature.integralHyst = 0;
					}
					break;
				case DECIMAL_HYSTERESIS:
					if (temperature.decimalHyst < MAX_DECIMAL_HYSTERESIS_TRESHOLD) {
						temperature.decimalHyst++;
					} else {
						temperature.decimalHyst = 0;
					}
					break;
			}
			
			loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
		}
		
		else if (button_down(BUTTON_DOWN_MASK)) {
			switch(cursorPosition) {
				case INTEGRAL_TEMPERATURE:
					if (temperature.integralT > 0) {
						temperature.integralT--;
					} else {
						temperature.integralT = MAX_INTEGRAL_TEMPERATURE_TRESHOLD;
					}
					break;
				case DECIMAL_TEMPERATURE:
					if (temperature.decimalT > 0) {
						temperature.decimalT--;
					} else {
						temperature.decimalT = MAX_DECIMAL_TEMPERATURE_TRESHOLD;
					}
					break;
				case INTEGRAL_HYSTERESIS:
					if (temperature.integralHyst > 0) {
						temperature.integralHyst--;
					} else {
						temperature.integralHyst = MAX_INTEGRAL_HYSTERESIS_TRESHOLD;
					}
					break;
				case DECIMAL_HYSTERESIS:
					if (temperature.decimalHyst > 0) {
						temperature.decimalHyst--;
					} else {
						temperature.decimalHyst = MAX_DECIMAL_HYSTERESIS_TRESHOLD;
					}
					break;
			}
						
			loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
		}
		
		else if (button_down(BUTTON_ENTER_MASK)) {
			if (cursorPosition < (INTEGRAL_TEMPERATURE + DECIMAL_HYSTERESIS)) {
				cursorPosition++;
			} else {
				cursorPosition = INTEGRAL_TEMPERATURE;
			}
			loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
		}
		
		else if (button_down(BUTTON_BACK_MASK)) {
			if (isTemperatureConfiguredCorrectly()) {
				switch(zoneNumber) {
				case ZONE_1:
					setupZoneTemperatureSettings(&zone1Settings);
					saveZoneSettingsToEEPROM(ZONE_1, &zone1Settings);
					break;
				case ZONE_2:
					setupZoneTemperatureSettings(&zone2Settings);
					saveZoneSettingsToEEPROM(ZONE_2, &zone2Settings);
					break;
				case ZONE_3:
					setupZoneTemperatureSettings(&zone3Settings);
					saveZoneSettingsToEEPROM(ZONE_3, &zone3Settings);
					break;
				case ZONE_4:
					setupZoneTemperatureSettings(&zone4Settings);
					saveZoneSettingsToEEPROM(ZONE_4, &zone4Settings);
					break;
				}
				needEscapeToMenu = true;
			}
			loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
		}
}


void setMaxTemperatureForZone1() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MAX_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_1;
	setupMenuTemperature(&zone1Settings.temperatureMax);
	displayZoneTemperatureSettings();
}

void setMinTemperatureForZone1() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MIN_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_1;
	setupMenuTemperature(&zone1Settings.temperatureMin);
	displayZoneTemperatureSettings();
}

void setMaxTemperatureForZone2() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MAX_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_2;
	setupMenuTemperature(&zone2Settings.temperatureMax);
	displayZoneTemperatureSettings();
}

void setMinTemperatureForZone2() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MIN_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_2;
	setupMenuTemperature(&zone2Settings.temperatureMin);
	displayZoneTemperatureSettings();
}

void setMaxTemperatureForZone3() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MAX_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_3;
	setupMenuTemperature(&zone3Settings.temperatureMax);
	displayZoneTemperatureSettings();
}

void setMinTemperatureForZone3() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MIN_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_3;
	setupMenuTemperature(&zone3Settings.temperatureMin);
	displayZoneTemperatureSettings();
}

void setMaxTemperatureForZone4() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MAX_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_4;
	setupMenuTemperature(&zone4Settings.temperatureMax);
	displayZoneTemperatureSettings();
}

void setMinTemperatureForZone4() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_TEMPERATURE;
	tempSettings = MIN_TEMPERATURE_SETTINGS;
	cursorPosition = INTEGRAL_TEMPERATURE;
	zoneNumber = ZONE_4;
	setupMenuTemperature(&zone4Settings.temperatureMin);
	displayZoneTemperatureSettings();
}

static void setupMenuTemperature(Temperature *temp) {
	temperature.integralT = temp -> integralT;
	temperature.decimalT = temp -> decimalT;
	temperature.integralHyst = temp -> integralHyst;
	temperature.decimalHyst = temp -> decimalHyst;
}

static void displayZoneTemperatureSettings() {
	clearLCD();
	enableInterruptTimer2();
	
	while (!needEscapeToMenu) {
		printLCDStringAtPosition(0, 0, getLabelString(L_TEMPERATURE));
		printfLCD(getLabelString(L_GENERIC_PATERN_3), temperature.integralT, temperature.decimalT);
		printLCDString(getLabelString(L_CELSIUS_DEGREE));
		printLCDString(getLabelString(L_EMPTY_LINE));
		printLCDStringAtPosition(1, 0, getLabelString(L_HYSTERESIS));
		printfLCD(getLabelString(L_GENERIC_PATERN_3), temperature.integralHyst, temperature.decimalHyst);
		printLCDString(getLabelString(L_CELSIUS_DEGREE));
		printLCDString(getLabelString(L_EMPTY_LINE));
		
		_delay_ms(DIGIT_BLINK_DELAY);
		
		if (needEscapeToMenu) {
			break;
		}
		
		switch(cursorPosition) {
		case INTEGRAL_TEMPERATURE:
			printLCDStringAtPosition(0, 0, getLabelString(L_TEMPERATURE));
			printfLCD(getLabelString(L_GENERIC_PATERN_NO_INTEGRAL_PART), temperature.decimalT);
			break;
			
		case DECIMAL_TEMPERATURE:
			printLCDStringAtPosition(0, 0, getLabelString(L_TEMPERATURE));
			printfLCD(getLabelString(L_GENERIC_PATERN_NO_DECIMAL_PART), temperature.integralT);
			break;
			
		case INTEGRAL_HYSTERESIS:
			printLCDStringAtPosition(1, 0, getLabelString(L_HYSTERESIS));
			printfLCD(getLabelString(L_GENERIC_PATERN_NO_INTEGRAL_PART), temperature.decimalHyst);
			break;
			
		case DECIMAL_HYSTERESIS:
			printLCDStringAtPosition(1, 0, getLabelString(L_HYSTERESIS));
			printfLCD(getLabelString(L_GENERIC_PATERN_NO_DECIMAL_PART), temperature.integralHyst);
			break;
		}
		
		if (needEscapeToMenu) {
			break;
		}
		
		_delay_ms(DIGIT_BLINK_DELAY);
	}
	
	disableInterruptTimer2();
	backToMenu();
}

static _Bool isTemperatureConfiguredCorrectly() {
	if ((temperature.integralT > 0 || temperature.decimalT > 0) && (temperature.integralHyst == 0 && temperature.decimalHyst == 0)) {// if temperature is set, then hysteresis also must be set
		clearLCD();
		printLCDStringAtPosition(0, 1, getLabelString(L_ERR_HYSTERESIS));
		printLCDStringAtPosition(1, 1, getLabelString(L_MUST_BE_PRESENT));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
		return false;
	}
	return true;
}

static void setupZoneTemperatureSettings(ZoneSettings *zoneSettings) {
	if (tempSettings == MAX_TEMPERATURE_SETTINGS) {
		zoneSettings -> temperatureMax.integralT = temperature.integralT;
		zoneSettings -> temperatureMax.decimalT = temperature.decimalT;
		zoneSettings -> temperatureMax.integralHyst = temperature.integralHyst;
		zoneSettings -> temperatureMax.decimalHyst = temperature.decimalHyst;
	} else {
		zoneSettings -> temperatureMin.integralT = temperature.integralT;
		zoneSettings -> temperatureMin.decimalT = temperature.decimalT;
		zoneSettings -> temperatureMin.integralHyst = temperature.integralHyst;
		zoneSettings -> temperatureMin.decimalHyst = temperature.decimalHyst;
	}
}
