#pragma once

#include "../../CommonSettings.h"
#include "../ZoneCommonSettings.h"

void processButtonsForZoneOnOffSettings();
void setOnOffForZone1();
void setOnOffForZone2();
void setOnOffForZone3();
void setOnOffForZone4();