#include "ZoneOnOffSettings.h"
#include "../../../MemoryModule/EEPROM.h"

static volatile _Bool isZoneSettingsEnabled = false;
static volatile uint8_t cursorPosition;

static void printOnOff();
static void saveZoneOnOffSetting(ZoneSettings *zoneSettings);

void processButtonsForZoneOnOffSettings() {
	
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		cursorPosition = 0;
		printOnOff();
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
	}
	
	else if (button_down(BUTTON_DOWN_MASK)) {
		cursorPosition = 1;
		printOnOff();
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
	}
	
	else if (button_down(BUTTON_ENTER_MASK)) {
		isZoneSettingsEnabled = cursorPosition == 0 ? true : false;
		
		switch(zoneNumber) {
			case ZONE_1:
				saveZoneOnOffSetting(&zone1Settings);
				break;
			case ZONE_2:
				saveZoneOnOffSetting(&zone2Settings);
				break;
			case ZONE_3:
				saveZoneOnOffSetting(&zone3Settings);
				break;
			case ZONE_4:
				saveZoneOnOffSetting(&zone4Settings);
				break;
		}
		
		printOnOff();
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
	}
	
	else if (button_down(BUTTON_BACK_MASK)) {
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
		disableInterruptTimer2();
		backToMenu();
	}
	
}

void setOnOffForZone1() {
	lockEnterButton();
	cursorPosition = 0;
	settingsItem = ZONE_SETTINGS_ON_OFF;
	zoneNumber = ZONE_1;
	isZoneSettingsEnabled = zone1Settings.isEnabled;
	enableInterruptTimer2();
	printOnOff();
}

void setOnOffForZone2() {
	lockEnterButton();
	cursorPosition = 0;
	settingsItem = ZONE_SETTINGS_ON_OFF;
	zoneNumber = ZONE_2;
	isZoneSettingsEnabled = zone2Settings.isEnabled;
	enableInterruptTimer2();
	printOnOff();
}

void setOnOffForZone3() {
	lockEnterButton();
	cursorPosition = 0;
	settingsItem = ZONE_SETTINGS_ON_OFF;
	zoneNumber = ZONE_3;
	isZoneSettingsEnabled = zone3Settings.isEnabled;
	enableInterruptTimer2();
	printOnOff();
}

void setOnOffForZone4() {
	lockEnterButton();
	cursorPosition = 0;
	settingsItem = ZONE_SETTINGS_ON_OFF;
	zoneNumber = ZONE_4;
	isZoneSettingsEnabled = zone4Settings.isEnabled;
	enableInterruptTimer2();
	printOnOff();
}

static void printOnOff() {
	clearLCD();
	
	if (cursorPosition == 0) {
		printLCDStringAtPosition(1, LCD_LAST_COLUMN_INDEX, getLabelString(L_WHITESPACE));
		goToXYLCD(0, LCD_LAST_COLUMN_INDEX);
		printLCDCustomCharacter(POINTER_TO_LEFT_SYMBOL_NUM);
	} else {
		printLCDStringAtPosition(0, LCD_LAST_COLUMN_INDEX, getLabelString(L_WHITESPACE));
		goToXYLCD(1, LCD_LAST_COLUMN_INDEX);
		printLCDCustomCharacter(POINTER_TO_LEFT_SYMBOL_NUM);
	}
	
	if (isZoneSettingsEnabled) {
		printLCDStringAtPosition(0, 3, getLabelString(L_ON));
		goToXYLCD(0, 6);
		printLCDCustomCharacter(ARROW_POINTER_SYMBOL_NUM);
		printLCDStringAtPosition(1, 3, getLabelString(L_OFF));
	} else {
		printLCDStringAtPosition(0, 3, getLabelString(L_ON));
		printLCDStringAtPosition(1, 3, getLabelString(L_OFF));
		goToXYLCD(1, 7);
		printLCDCustomCharacter(ARROW_POINTER_SYMBOL_NUM);
	}
}

static void saveZoneOnOffSetting(ZoneSettings *zoneSettings) {
	if (zoneSettings -> isEnabled != isZoneSettingsEnabled) {
		zoneSettings -> isEnabled = isZoneSettingsEnabled;
		saveZoneSettingsToEEPROM(zoneNumber, zoneSettings);
	}
}