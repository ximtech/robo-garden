#pragma once

#include "../CommonSettings.h"
#include <stdint.h>

typedef enum ZoneNumber {
	ZONE_1 = 1,
	ZONE_2 = 2,
	ZONE_3 = 3,
	ZONE_4 = 4
} ZoneNumber;

typedef struct Timer {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
} Timer;

typedef struct WateringTimer {
	Timer startTimer;
	Timer endTimer;
	_Bool weekDays[7];
} WateringTimer;

typedef struct LightTimer {
	Timer startTimer;
	Timer endTimer;
	_Bool weekDays[7];
} LightTimer;

typedef struct Temperature {
	int8_t integralT;	// temperature can be negative, setting signed int
	uint8_t decimalT;
	uint8_t integralHyst;
	uint8_t decimalHyst;
} Temperature;

typedef struct Soil {
	uint8_t sensorCount;
	uint8_t humidityPercentage;
} Soil;

typedef struct ZoneSettings {
	ZoneNumber zoneNumber;
	_Bool isEnabled;
	Soil soilData;
	Temperature temperatureMax;
	Temperature temperatureMin;
	WateringTimer waterTimer1;
	WateringTimer waterTimer2;
	WateringTimer waterTimer3;
	LightTimer lightTimer1;
	LightTimer lightTimer2;
	LightTimer lightTimer3;
} ZoneSettings;

typedef enum TimerTimeSettingMode {
	SET_HOURS_ON,
	SET_MINUTES_ON,
	SET_SECONDS_ON,
	SET_HOURS_OFF,
	SET_MINUTES_OFF,
	SET_SECONDS_OFF
} TimerTimeSettingMode;

typedef enum TimerType {
	TIMER1_WATERING,
	TIMER2_WATERING,
	TIMER3_WATERING,
	TIMER1_LIGHT,
	TIMER2_LIGHT,
	TIMER3_LIGHT,
	TIMER1_WATERING_WEEK_DAY,
	TIMER2_WATERING_WEEK_DAY,
	TIMER3_WATERING_WEEK_DAY,
	TIMER1_LIGHT_WEEK_DAY,
	TIMER2_LIGHT_WEEK_DAY,
	TIMER3_LIGHT_WEEK_DAY
} TimerType;

ZoneNumber zoneNumber;
ZoneSettings zone1Settings;
ZoneSettings zone2Settings;
ZoneSettings zone3Settings;
ZoneSettings zone4Settings;

volatile TimerType timerType;