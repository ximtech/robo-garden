#pragma once

#include "../../CommonSettings.h"
#include "../ZoneCommonSettings.h"
#include "../../../SensorModule/DHTModule.h"
#include "../../../SensorModule/SoilModule.h"

void processBackButtonForZoneDHTView();

void viewZone1SensorData();
void viewZone2SensorData();
void viewZone3SensorData();
void viewZone4SensorData();