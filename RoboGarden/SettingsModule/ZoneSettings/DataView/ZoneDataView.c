#include "ZoneDataView.h"

static volatile _Bool needEscapeToMenu = false;

static void displayZoneDHTdata(ZoneNumber zoneNumber);

void processBackButtonForZoneDHTView() {
	
	debounce();
	
	if (button_down(BUTTON_BACK_MASK)) {
		needEscapeToMenu = true;
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
}

void viewZone1SensorData() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_VIEW_DHT;
	enableInterruptTimer2();
	displayZoneDHTdata(ZONE_1);
}

void viewZone2SensorData() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_VIEW_DHT;
	enableInterruptTimer2();
	displayZoneDHTdata(ZONE_2);
}

void viewZone3SensorData() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_VIEW_DHT;
	enableInterruptTimer2();
	displayZoneDHTdata(ZONE_3);
}

void viewZone4SensorData() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_VIEW_DHT;
	enableInterruptTimer2();
	displayZoneDHTdata(ZONE_4);
}

static void displayZoneDHTdata(ZoneNumber zoneNumber) {
	
	if (zoneNumber == ZONE_1 && !zone1Settings.isEnabled) {
		needEscapeToMenu = true;
		clearLCD();
		printLCDStringAtPosition(0, 1, getLabelString(L_ERR_ZONE_1));
		printLCDStringAtPosition(1, 1, getLabelString(L_ERR_NOT_ENABLED));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
		
	} else if (zoneNumber == ZONE_2 && !zone2Settings.isEnabled) {
		needEscapeToMenu = true;
		clearLCD();
		printLCDStringAtPosition(0, 1, getLabelString(L_ERR_ZONE_2));
		printLCDStringAtPosition(1, 1, getLabelString(L_ERR_NOT_ENABLED));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
		
	}  else if (zoneNumber == ZONE_3 && !zone3Settings.isEnabled) {
		needEscapeToMenu = true;
		clearLCD();
		printLCDStringAtPosition(0, 1, getLabelString(L_ERR_ZONE_3));
		printLCDStringAtPosition(1, 1, getLabelString(L_ERR_NOT_ENABLED));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
		
	}  else if (zoneNumber == ZONE_4 && !zone4Settings.isEnabled) {
		needEscapeToMenu = true;
		clearLCD();
		printLCDStringAtPosition(0, 1, getLabelString(L_ERR_ZONE_4));
		printLCDStringAtPosition(1, 1, getLabelString(L_ERR_NOT_ENABLED));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
	}
	
	clearLCD();
	volatile ZoneDHTData *zdtData;
	while(!needEscapeToMenu) {
		zdtData = getZoneDHTData(zoneNumber);
		
		if (zdtData -> sensorStatus == DHT_SENSOR_OK) {
			goToXYLCD(0, 0);
			printfLCD(getLabelString(L_RH_TEM_PATTERN), zdtData -> integralRH, zdtData -> decimalRH, zdtData -> integralT, zdtData -> decimalT);
			printLCDString(getLabelString(L_CELSIUS_DEGREE));
			printLCDStringAtPosition(1, 0, getLabelString(L_SOIL_MOISTURE));
			
			uint16_t soilMoisturePercentage = 0;
			if (isSoilSensorEnabled(zoneNumber)) {
				soilMoisturePercentage = readZoneSoilMoisureInPercentages(zoneNumber);
			}
			printfLCD(getLabelString(L_GENERIC_PATERN_4), soilMoisturePercentage);
			printLCDString(getLabelString(L_PERCENTAGE));
			printLCDString(getLabelString(L_TAB));
		}
		else if (zdtData -> sensorStatus == DHT_SENSOR_NOT_CONNECTED) {
			printLCDStringAtPosition(0, 0, getLabelString(L_DHT_SENSOR));
			printLCDStringAtPosition(1, 0, getLabelString(L_DHT_NOT_CONNECTED_LONG));
		} else {
			printLCDStringAtPosition(0, 0, getLabelString(L_DHT_SENSOR));
			printLCDStringAtPosition(1, 0, getLabelString(L_DHT_BROKEN_ERROR_LONG));
		}
		_delay_ms(1000);
	}
	
	disableInterruptTimer2();
	backToMenu();
}