#include "ZoneSoilSettings.h"

#define SENSOR_COUNT_VALUE_LCD_POSITION 14
#define MOISTURE_VALUE_LCD_POSITION     10
#define MAX_SENSOR_COUNT 4
#define MAX_MOISTURE_PERCENTAGE 100

static volatile _Bool needEscapeToMenu;

static volatile enum CursorPosition {
	SOIL_MOISTURE_PERCENTAGE,
	SOIL_SENSOR_COUNT
} cursorPosition;

static volatile Soil soil = { .humidityPercentage = 0, .sensorCount = 0 };

static void setupMenuSoilSettings(Soil *soilData);
static void displayZoneSoilSettings();
static void setupZoneSoilSettings(Soil *zoneSoilSettings);


void processButtonsForZoneSoilSettings() {
			
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		switch(cursorPosition) {
			case SOIL_MOISTURE_PERCENTAGE:
				if (soil.humidityPercentage < MAX_MOISTURE_PERCENTAGE) {
					soil.humidityPercentage++;
				} else {
					soil.humidityPercentage = 0;
				}
				_delay_ms(20);
				break;
			
			case SOIL_SENSOR_COUNT:
				if (soil.sensorCount < MAX_SENSOR_COUNT) {
					soil.sensorCount++;
				} else {
					soil.sensorCount = 0;
				}
				loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
				break;
		}
	}
	
	else if (button_down(BUTTON_DOWN_MASK)) {
		switch(cursorPosition) {
			case SOIL_MOISTURE_PERCENTAGE:
				if (soil.humidityPercentage > 0) {
					soil.humidityPercentage--;
				} else {
					soil.humidityPercentage = MAX_MOISTURE_PERCENTAGE;
				}
				_delay_ms(20);
				break;
					
			case SOIL_SENSOR_COUNT:
				if (soil.sensorCount > 0) {
					soil.sensorCount--;
				} else {
					soil.sensorCount = MAX_SENSOR_COUNT;
				}
				loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
				break;
		}
	}
	
	else if (button_down(BUTTON_ENTER_MASK)) {
		if (cursorPosition < SOIL_SENSOR_COUNT) {
			cursorPosition++;
		} else {
			cursorPosition = SOIL_MOISTURE_PERCENTAGE;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
	
	else if (button_down(BUTTON_BACK_MASK)) {
		switch(zoneNumber) {
			case ZONE_1:
				setupZoneSoilSettings(&zone1Settings.soilData);
				saveZoneSettingsToEEPROM(ZONE_1, &zone1Settings);
				break;
			case ZONE_2:
				setupZoneSoilSettings(&zone2Settings.soilData);
				saveZoneSettingsToEEPROM(ZONE_2, &zone2Settings);
				break;
			case ZONE_3:
				setupZoneSoilSettings(&zone3Settings.soilData);
				saveZoneSettingsToEEPROM(ZONE_3, &zone3Settings);
				break;
			case ZONE_4:
				setupZoneSoilSettings(&zone4Settings.soilData);
				saveZoneSettingsToEEPROM(ZONE_4, &zone4Settings);
				break;
		}
		needEscapeToMenu = true;
	
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
}


void setZone1SoilSettings() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_SOIL;
	cursorPosition = SOIL_SENSOR_COUNT;
	zoneNumber = ZONE_1;
	setupMenuSoilSettings(&zone1Settings.soilData);
	displayZoneSoilSettings();
}

void setZone2SoilSettings() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_SOIL;
	cursorPosition = SOIL_SENSOR_COUNT;
	zoneNumber = ZONE_2;
	setupMenuSoilSettings(&zone2Settings.soilData);
	displayZoneSoilSettings();
}

void setZone3SoilSettings() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_SOIL;
	cursorPosition = SOIL_SENSOR_COUNT;
	zoneNumber = ZONE_3;
	setupMenuSoilSettings(&zone3Settings.soilData);
	displayZoneSoilSettings();
}

void setZone4SoilSettings() {
	lockEnterButton();
	needEscapeToMenu = false;
	settingsItem = ZONE_SETTINGS_SOIL;
	cursorPosition = SOIL_SENSOR_COUNT;
	zoneNumber = ZONE_4;
	setupMenuSoilSettings(&zone4Settings.soilData);
	displayZoneSoilSettings();
}

static void setupMenuSoilSettings(Soil *soilData) {
	soil.humidityPercentage = soilData -> humidityPercentage;
	soil.sensorCount = soilData -> sensorCount;
}

static void displayZoneSoilSettings() {
	clearLCD();
	enableInterruptTimer2();
	
	while(!needEscapeToMenu) {
		printLCDStringAtPosition(0, 0, getLabelString(L_SOIL_SENSOR_COUNT));
		printfLCD(getLabelString(L_GENERIC_PATERN_4), soil.sensorCount);
		printLCDString(getLabelString(L_WHITESPACE));
		printLCDStringAtPosition(1, 0, getLabelString(L_SOIL_MOISTURE));
		printfLCD(getLabelString(L_GENERIC_PATERN_4), soil.humidityPercentage);
		printLCDString(getLabelString(L_PERCENTAGE));
		printLCDString(getLabelString(L_WHITESPACE));
		
		_delay_ms(DIGIT_BLINK_DELAY);
		
		switch(cursorPosition) {
			case SOIL_SENSOR_COUNT:
				printLCDStringAtPosition(0, SENSOR_COUNT_VALUE_LCD_POSITION, getLabelString(L_EMPTY_LINE));
				break;
				
			case SOIL_MOISTURE_PERCENTAGE:
				printLCDStringAtPosition(1, MOISTURE_VALUE_LCD_POSITION, getLabelString(L_EMPTY_LINE));
				break;
		}
		
		if (needEscapeToMenu) {
			break;
		}
		
		_delay_ms(DIGIT_BLINK_DELAY);
	}
	
	disableInterruptTimer2();
	backToMenu();
}

static void setupZoneSoilSettings(Soil *zoneSoilSettings) {
	zoneSoilSettings -> humidityPercentage = soil.humidityPercentage;
	zoneSoilSettings -> sensorCount = soil.sensorCount;
}