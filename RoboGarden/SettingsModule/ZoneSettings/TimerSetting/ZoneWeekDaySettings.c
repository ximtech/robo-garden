#include "ZoneWeekDaySettings.h"

#define ALL_DAYS 0
#define WEEK_LABEL_LENGTH 8
#define WEEK_DAY_LABEL_MAX_LENGTH 10
#define WEEK_DAY_LABEL_COL_POSITION 2

static volatile _Bool needToEscapeToMenu = false;

static volatile uint8_t upperCursor;
static volatile uint8_t lowerCursor;
static volatile _Bool isMenuAlreadyViewed = false;

const static Labels weekLabels[] = {
	L_EVERY_DAY,
	L_SUNDAY_FULL_NAME,
	L_MONDAY_FULL_NAME,
	L_TUESDAY_FULL_NAME,
	L_WEDNESDAY_FULL_NAME,
	L_THURSDAY_FULL_NAME,
	L_FRIDAY_FULL_NAME,
	L_SATURDAY_FULL_NAME
};

static volatile _Bool weekDaySet[] = { // list of enabled week days
	true,	// EVERY DAY
	false,	// SUNDAY
	false,	// MONDAY
	false,	// TUESDAY
	false,	// WEDNESDAY
	false,	// THURSDAY
	false,	// FRIDAY
	false	// SATURDAY
};

static void processSetTimerWeekDays(ZoneSettings *zoneSettings, TimerType zoneTimerType);
static void displayWeekDaySettings();
static void setWeekDay();
static void setZoneRoutineWeekDays(ZoneSettings *zoneSettings);
static void setAllWeekDaySetArrayValuesTo(_Bool flag);
static void setTimerWorkingWeekDays(_Bool *weekDayArray);
static void initZoneWeekDaySettings(ZoneSettings *zoneSettings);
static void initWeekDaySetFromZoneSetting(_Bool *weekDayArray);
static void goBackToMenu();

void processButtonsForZoneWeekDaySettings() {
	
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		if (upperCursor > 0) {
			upperCursor--;
			lowerCursor--;
			isMenuAlreadyViewed = false;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);

	} 
	
	else if (button_down(BUTTON_DOWN_MASK)) {
		if (lowerCursor <= WEEK_LABEL_LENGTH) {
			upperCursor++;
			lowerCursor++;
			isMenuAlreadyViewed = false;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
		
	} 
	
	else if (button_down(BUTTON_ENTER_MASK)) {
		setWeekDay();
		isMenuAlreadyViewed = false;
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
		
	} 
	
	else if (button_down(BUTTON_BACK_MASK)) {
		
		switch(zoneNumber) {
			case ZONE_1:
				setZoneRoutineWeekDays(&zone1Settings);
				saveZoneSettingsToEEPROM(ZONE_1, &zone1Settings);
				break;
			case ZONE_2:
				setZoneRoutineWeekDays(&zone2Settings);
				saveZoneSettingsToEEPROM(ZONE_2, &zone2Settings);
				break;
			case ZONE_3:
				setZoneRoutineWeekDays(&zone3Settings);
				saveZoneSettingsToEEPROM(ZONE_3, &zone3Settings);
				break;
			case ZONE_4:
				setZoneRoutineWeekDays(&zone4Settings);
				saveZoneSettingsToEEPROM(ZONE_4, &zone4Settings);
				break;
		}
		
		needToEscapeToMenu = true;
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
	
}

// -------------------- Zone 1 ----------------------------------
void setTimer1WateringWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER1_WATERING_WEEK_DAY);
}

void setTimer2WateringWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER2_WATERING_WEEK_DAY);
}

void setTimer3WateringWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER3_WATERING_WEEK_DAY);
}

void setTimer1LightWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER1_LIGHT_WEEK_DAY);
}

void setTimer2LightWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER2_LIGHT_WEEK_DAY);
}

void setTimer3LightWeekdaysForZone1() {
	lockEnterButton();
	processSetTimerWeekDays(&zone1Settings, TIMER3_LIGHT_WEEK_DAY);
}
// -------------------- End Zone 1 ----------------------------------


// -------------------- Zone 2 ----------------------------------
void setTimer1WateringWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER1_WATERING_WEEK_DAY);
}

void setTimer2WateringWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER2_WATERING_WEEK_DAY);
}

void setTimer3WateringWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER3_WATERING_WEEK_DAY);
}

void setTimer1LightWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER1_LIGHT_WEEK_DAY);
}

void setTimer2LightWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER2_LIGHT_WEEK_DAY);
}

void setTimer3LightWeekdaysForZone2() {
	lockEnterButton();
	processSetTimerWeekDays(&zone2Settings, TIMER3_LIGHT_WEEK_DAY);
}
// -------------------- End Zone 2 ----------------------------------

// -------------------- Zone 3 ----------------------------------
void setTimer1WateringWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER1_WATERING_WEEK_DAY);
}

void setTimer2WateringWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER2_WATERING_WEEK_DAY);
}

void setTimer3WateringWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER3_WATERING_WEEK_DAY);
}

void setTimer1LightWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER1_LIGHT_WEEK_DAY);
}

void setTimer2LightWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER2_LIGHT_WEEK_DAY);
}

void setTimer3LightWeekdaysForZone3() {
	lockEnterButton();
	processSetTimerWeekDays(&zone3Settings, TIMER3_LIGHT_WEEK_DAY);
}
// -------------------- End Zone 3 ----------------------------------

// -------------------- Zone 4 ----------------------------------
void setTimer1WateringWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER1_WATERING_WEEK_DAY);
}

void setTimer2WateringWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER2_WATERING_WEEK_DAY);
}

void setTimer3WateringWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER3_WATERING_WEEK_DAY);
}

void setTimer1LightWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER1_LIGHT_WEEK_DAY);
}

void setTimer2LightWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER2_LIGHT_WEEK_DAY);
}

void setTimer3LightWeekdaysForZone4() {
	lockEnterButton();
	processSetTimerWeekDays(&zone4Settings, TIMER3_LIGHT_WEEK_DAY);
}
// -------------------- End Zone 4 ----------------------------------

static void processSetTimerWeekDays(ZoneSettings *zoneSettings, TimerType zoneTimerType) {
	timerType = zoneTimerType;
	settingsItem = ZONE_SETTINGS_WEEK_DAY;
	zoneNumber = zoneSettings -> zoneNumber;
	
	if (isZoneTimerInProcess(zoneSettings, zoneTimerType)) {
		goBackToMenu();
		return;
	}
	
	upperCursor = 0;
	lowerCursor = MENU_MAX_ITEM_VIEW;
	initZoneWeekDaySettings(zoneSettings);
	enableInterruptTimer2();
	displayWeekDaySettings();
}

static void displayWeekDaySettings() {
	while(!needToEscapeToMenu) {
		
		if (!isMenuAlreadyViewed) {
			clearLCD();
			goToXYLCD(0, LCD_COL_COUNT - 1);
			printLCDCustomCharacter(POINTER_TO_LEFT_SYMBOL_NUM);
			
			for (uint8_t i = upperCursor; i < lowerCursor; i++) {
				if (i < WEEK_LABEL_LENGTH) {
					if (weekDaySet[i]) {
						goToXYLCD((i - upperCursor), WEEK_DAY_LABEL_COL_POSITION + WEEK_DAY_LABEL_MAX_LENGTH);
						printLCDCustomCharacter(ARROW_POINTER_SYMBOL_NUM);
					}
					goToXYLCD((i - upperCursor), WEEK_DAY_LABEL_COL_POSITION);
					printLCDString( getLabelString(weekLabels[i]) );
						
					} else {
						goToXYLCD((i - upperCursor), WEEK_DAY_LABEL_COL_POSITION);
						printLCDString(getLabelString(L_WHITESPACE));
				}
			}
			isMenuAlreadyViewed = true;
		}
	}
	
	goBackToMenu();
}

static void setWeekDay() {
	if (upperCursor == ALL_DAYS) {
		weekDaySet[ALL_DAYS] = weekDaySet[ALL_DAYS] ? false : true;
		if (weekDaySet[ALL_DAYS]) {
			setAllWeekDaySetArrayValuesTo(false);
		} else {
			setAllWeekDaySetArrayValuesTo(true);
		}
	} else {
		weekDaySet[upperCursor] = weekDaySet[upperCursor] ? false : true;
		weekDaySet[ALL_DAYS] = false;
	}
}

static void setZoneRoutineWeekDays(ZoneSettings *zoneSettings) {
	if (timerType == TIMER1_WATERING_WEEK_DAY) {
		setTimerWorkingWeekDays(zoneSettings -> waterTimer1.weekDays);
	} else if (timerType == TIMER2_WATERING_WEEK_DAY) {
		setTimerWorkingWeekDays(zoneSettings -> waterTimer2.weekDays);
	} else if (timerType == TIMER3_WATERING_WEEK_DAY) {
		setTimerWorkingWeekDays(zoneSettings -> waterTimer3.weekDays);
	} else if (timerType == TIMER1_LIGHT_WEEK_DAY) {
		setTimerWorkingWeekDays(zone1Settings.lightTimer1.weekDays);
	} else if (timerType == TIMER2_LIGHT_WEEK_DAY) {
		setTimerWorkingWeekDays(zoneSettings -> lightTimer2.weekDays);
	} else if (timerType == TIMER3_LIGHT_WEEK_DAY) {
		setTimerWorkingWeekDays(zoneSettings -> lightTimer3.weekDays);
	}
}

static void setAllWeekDaySetArrayValuesTo(_Bool flag) {
	for (uint8_t i = (ALL_DAYS + 1); i < WEEK_LABEL_LENGTH; i++) {
		weekDaySet[i] = flag;
	}
}

static void setTimerWorkingWeekDays(_Bool *weekDayArray) {
	if (weekDaySet[ALL_DAYS]) {										// if all days selected, then set all week days in array to true
		for (uint8_t i = 0; i < DAYS_IN_WEEK; i++) {
			weekDayArray[i] = true;
		}
	} else {
		for (uint8_t i = 0; i < DAYS_IN_WEEK; i++) {		// else add only chosen week days
			weekDayArray[i] = weekDaySet[i + 1];
		}
	}
}

static void initZoneWeekDaySettings(ZoneSettings *zoneSettings) {
	if (timerType == TIMER1_WATERING_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> waterTimer1.weekDays);
	} else if (timerType == TIMER2_WATERING_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> waterTimer2.weekDays);
	} else if (timerType == TIMER3_WATERING_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> waterTimer3.weekDays);
	} else if (timerType == TIMER1_LIGHT_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> lightTimer1.weekDays);
	} else if (timerType == TIMER2_LIGHT_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> lightTimer2.weekDays);
	} else if (timerType == TIMER3_LIGHT_WEEK_DAY) {
		initWeekDaySetFromZoneSetting(zoneSettings -> lightTimer3.weekDays);
	}
}

static void initWeekDaySetFromZoneSetting(_Bool *weekDayArray) {
	_Bool isAllWeekDaysSetToTrue = true;
	for (uint8_t i = 0; i < DAYS_IN_WEEK; i++) {	// check that all days is set
		if (!weekDayArray[i]) {
			isAllWeekDaysSetToTrue = false;
		}
	}
	
	if (isAllWeekDaysSetToTrue) {
		weekDaySet[ALL_DAYS] = true;				// every day set to true
		setAllWeekDaySetArrayValuesTo(false);		// other day to false
	} else {
		weekDaySet[ALL_DAYS] = false;
		for (uint8_t i = 0; i < DAYS_IN_WEEK; i++) {	// set value from zone settings
			weekDaySet[i + 1] = weekDayArray[i];
		}
	}
}

static void goBackToMenu() {
	needToEscapeToMenu = false;
	isMenuAlreadyViewed = false;
	disableInterruptTimer2();
	backToMenu();
}
