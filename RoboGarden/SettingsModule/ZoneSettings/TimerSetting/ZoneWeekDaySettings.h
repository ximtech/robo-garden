#pragma once

#include "../../../MemoryModule/EEPROM.h"
#include "ZoneValidations.h"

void processButtonsForZoneWeekDaySettings();

// Zone 1
void setTimer1WateringWeekdaysForZone1();
void setTimer2WateringWeekdaysForZone1();
void setTimer3WateringWeekdaysForZone1();

void setTimer1LightWeekdaysForZone1();
void setTimer2LightWeekdaysForZone1();
void setTimer3LightWeekdaysForZone1();

// Zone 2
void setTimer1WateringWeekdaysForZone2();
void setTimer2WateringWeekdaysForZone2();
void setTimer3WateringWeekdaysForZone2();

void setTimer1LightWeekdaysForZone2();
void setTimer2LightWeekdaysForZone2();
void setTimer3LightWeekdaysForZone2();

// Zone 3
void setTimer1WateringWeekdaysForZone3();
void setTimer2WateringWeekdaysForZone3();
void setTimer3WateringWeekdaysForZone3();

void setTimer1LightWeekdaysForZone3();
void setTimer2LightWeekdaysForZone3();
void setTimer3LightWeekdaysForZone3();

// Zone 4
void setTimer1WateringWeekdaysForZone4();
void setTimer2WateringWeekdaysForZone4();
void setTimer3WateringWeekdaysForZone4();

void setTimer1LightWeekdaysForZone4();
void setTimer2LightWeekdaysForZone4();
void setTimer3LightWeekdaysForZone4();