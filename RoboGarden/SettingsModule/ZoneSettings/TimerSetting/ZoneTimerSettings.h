#pragma once

#include "../../../MemoryModule/EEPROM.h"
#include "ZoneValidations.h"

void processButtonsForZoneTimeSettings();

// Zone 1
void setTimer1WateringForZone1();
void setTimer2WateringForZone1();
void setTimer3WateringForZone1();

void setTimer1LightForZone1();
void setTimer2LightForZone1();
void setTimer3LightForZone1();

// Zone 2
void setTimer1WateringForZone2();
void setTimer2WateringForZone2();
void setTimer3WateringForZone2();

void setTimer1LightForZone2();
void setTimer2LightForZone2();
void setTimer3LightForZone2();

// Zone 3
void setTimer1WateringForZone3();
void setTimer2WateringForZone3();
void setTimer3WateringForZone3();

void setTimer1LightForZone3();
void setTimer2LightForZone3();
void setTimer3LightForZone3();

// Zone 4
void setTimer1WateringForZone4();
void setTimer2WateringForZone4();
void setTimer3WateringForZone4();

void setTimer1LightForZone4();
void setTimer2LightForZone4();
void setTimer3LightForZone4();