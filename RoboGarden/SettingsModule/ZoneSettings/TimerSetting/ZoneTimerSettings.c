#include "ZoneTimerSettings.h"

static Timer innerStartTimerHolder = { .hours = 0, .minutes = 0, .seconds = 0 };
static Timer innerEndTimerHolder = { .hours = 0, .minutes = 0, .seconds = 0 };
static volatile TimerTimeSettingMode dateTimeSettingMode = SET_HOURS_ON;
static volatile _Bool needToEscapeToMenu = false;
static volatile _Bool isMenuAlreadyViewed = false;

void processSetTimerTime(ZoneSettings *zoneSettings, TimerType zoneTimerType);
static void dispalyTimeOn();
static void displayTimeOff();
static void displayZoneTimerSettings();

static void setInnerTimeHolder(Timer *startTimer, Timer *endTimer);
static void incrementHours(Timer *timer);
static void incrementMinutes(Timer *timer);
static void incrementSeconds(Timer *timer);

static void decrementHours(Timer *timer);
static void decrementMinutes(Timer *timer);
static void decrementSeconds(Timer *timer);

static _Bool isStartTimeBeforEndTime();
static void saveZoneTimerSettings(ZoneSettings *zoneSettings);
static void saveTimerValues(Timer *startTimer, Timer *endTimer);
static void goBackToMenu();


void processButtonsForZoneTimeSettings() {
	
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		switch(dateTimeSettingMode) {
			case SET_HOURS_ON:
				incrementHours(&innerStartTimerHolder);
				break;
			case SET_MINUTES_ON:
				incrementMinutes(&innerStartTimerHolder);
				break;
			case SET_SECONDS_ON:
				incrementSeconds(&innerStartTimerHolder);
				break;
			case SET_HOURS_OFF:
				incrementHours(&innerEndTimerHolder);
				break;
			case SET_MINUTES_OFF:
				incrementMinutes(&innerEndTimerHolder);
				break;
			case SET_SECONDS_OFF:
				incrementSeconds(&innerEndTimerHolder);
				break;
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
	}
	
	else if (button_down(BUTTON_DOWN_MASK)) {
		switch(dateTimeSettingMode) {
			case SET_HOURS_ON:
				decrementHours(&innerStartTimerHolder);
				break;
			case SET_MINUTES_ON:
				decrementMinutes(&innerStartTimerHolder);
				break;
			case SET_SECONDS_ON:
				decrementSeconds(&innerStartTimerHolder);
				break;
			case SET_HOURS_OFF:
				decrementHours(&innerEndTimerHolder);
				break;
			case SET_MINUTES_OFF:
				decrementMinutes(&innerEndTimerHolder);
				break;
			case SET_SECONDS_OFF:
				decrementSeconds(&innerEndTimerHolder);
				break;
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
	}
	
	else if (button_down(BUTTON_ENTER_MASK)) {
		if (dateTimeSettingMode < (SET_HOURS_ON + SET_SECONDS_OFF)) {
			dateTimeSettingMode++;
		} else {
			dateTimeSettingMode = SET_HOURS_ON;
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
	}
	
	else if (button_down(BUTTON_BACK_MASK)) {
		if (isStartTimeBeforEndTime(&innerStartTimerHolder, &innerEndTimerHolder)) {	// if timer time is correctly set, then save to eeprom
			switch(zoneNumber) {
				case ZONE_1:
					saveZoneTimerSettings(&zone1Settings);
					break;
				case ZONE_2:
					saveZoneTimerSettings(&zone2Settings);
					break;
				case ZONE_3:
					saveZoneTimerSettings(&zone3Settings);
					break;
				case ZONE_4:
					saveZoneTimerSettings(&zone4Settings);
					break;
			}
			needToEscapeToMenu = true;
		}
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
	
}


// -------------------- Zone 1 ----------------------------------
void setTimer1WateringForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER1_WATERING);
}

void setTimer2WateringForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER2_WATERING);
}

void setTimer3WateringForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER3_WATERING);
}

void setTimer1LightForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER1_LIGHT);
}

void setTimer2LightForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER2_LIGHT);
}

void setTimer3LightForZone1() {
	lockEnterButton();
	processSetTimerTime(&zone1Settings, TIMER3_LIGHT);
}
// -------------------- End Zone 1 ----------------------------------

// -------------------- Zone 2 ----------------------------------
void setTimer1WateringForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER1_WATERING);
}

void setTimer2WateringForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER2_WATERING);
}

void setTimer3WateringForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER3_WATERING);
}

void setTimer1LightForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER1_LIGHT);
}

void setTimer2LightForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER2_LIGHT);
}

void setTimer3LightForZone2() {
	lockEnterButton();
	processSetTimerTime(&zone2Settings, TIMER3_LIGHT);
}
// -------------------- End Zone 2 ----------------------------------

// -------------------- Zone 3 ----------------------------------
void setTimer1WateringForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER1_WATERING);
}

void setTimer2WateringForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER2_WATERING);
}

void setTimer3WateringForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER3_WATERING);
}

void setTimer1LightForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER1_LIGHT);
}

void setTimer2LightForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER2_LIGHT);
}

void setTimer3LightForZone3() {
	lockEnterButton();
	processSetTimerTime(&zone3Settings, TIMER3_LIGHT);
}
// -------------------- End Zone 3 ----------------------------------

// -------------------- Zone 4 ----------------------------------
void setTimer1WateringForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER1_WATERING);
}

void setTimer2WateringForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER2_WATERING);
}

void setTimer3WateringForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER3_WATERING);
}

void setTimer1LightForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER1_LIGHT);
}

void setTimer2LightForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER2_LIGHT);
}

void setTimer3LightForZone4() {
	lockEnterButton();
	processSetTimerTime(&zone4Settings, TIMER3_LIGHT);
}
// -------------------- End Zone 4 ----------------------------------


void processSetTimerTime(ZoneSettings *zoneSettings, TimerType zoneTimerType) {
	timerType = zoneTimerType;
	settingsItem = ZONE_SETTINGS_TIME;
	zoneNumber = zoneSettings -> zoneNumber;
	
	if (isZoneTimerInProcess(zoneSettings, zoneTimerType)) {
		goBackToMenu();
		return;
	}
	
	needToEscapeToMenu = false;
	if (zoneTimerType == TIMER1_WATERING) {
		setInnerTimeHolder(&zoneSettings -> waterTimer1.startTimer, &zoneSettings -> waterTimer1.endTimer);
		displayZoneTimerSettings();
		
	} else if (zoneTimerType == TIMER2_WATERING) {
		setInnerTimeHolder(&zoneSettings -> waterTimer2.startTimer, &zoneSettings -> waterTimer2.endTimer);
		displayZoneTimerSettings();
		
	} else if (zoneTimerType == TIMER3_WATERING) {
		setInnerTimeHolder(&zoneSettings -> waterTimer3.startTimer, &zoneSettings -> waterTimer3.endTimer);
		displayZoneTimerSettings();
		
	} else if (zoneTimerType == TIMER1_LIGHT) {
		setInnerTimeHolder(&zoneSettings -> lightTimer1.startTimer, &zoneSettings -> lightTimer1.endTimer);
		displayZoneTimerSettings();
		
	} else if (zoneTimerType == TIMER2_LIGHT) {
		setInnerTimeHolder(&zoneSettings -> lightTimer2.startTimer, &zoneSettings -> lightTimer2.endTimer);
		displayZoneTimerSettings();
		
	} else if (zoneTimerType == TIMER3_LIGHT) {
		setInnerTimeHolder(&zoneSettings -> lightTimer3.startTimer, &zoneSettings -> lightTimer3.endTimer);
		displayZoneTimerSettings();
		
	} else {
		goBackToMenu();	// unknown timer type
	}
}

void setInnerTimeHolder(Timer *startTimer, Timer *endTimer) {
	innerStartTimerHolder.hours = startTimer -> hours;
	innerStartTimerHolder.minutes = startTimer -> minutes;
	innerStartTimerHolder.seconds = startTimer -> seconds;
	innerEndTimerHolder.hours = endTimer -> hours;
	innerEndTimerHolder.minutes = endTimer -> minutes;
	innerEndTimerHolder.seconds = endTimer -> seconds;
}

static void displayZoneTimerSettings() {
		clearLCD();
		dateTimeSettingMode = SET_HOURS_ON;
		enableInterruptTimer2();
		
		while (!needToEscapeToMenu) {
			
			dispalyTimeOn();
			displayTimeOff();
			
			_delay_ms(DIGIT_BLINK_DELAY);
			
			switch(dateTimeSettingMode) {
				case SET_HOURS_ON:
					goToXYLCD(0, 0);
					printfLCD(getLabelString(L_TIMER_TIME_ON_NO_HOURS_PATTERN), innerStartTimerHolder.minutes, innerStartTimerHolder.seconds);
					break;
				
				case SET_MINUTES_ON:
					goToXYLCD(0, 0);
					printfLCD(getLabelString(L_TIMER_TIME_ON_NO_MINUTES_PATTERN), innerStartTimerHolder.hours, innerStartTimerHolder.seconds);
					break;
				
				case SET_SECONDS_ON:
					goToXYLCD(0, 0);
					printfLCD(getLabelString(L_TIMER_TIME_ON_NO_SECONDS_PATTERN), innerStartTimerHolder.hours, innerStartTimerHolder.minutes);
					break;
				
				case SET_HOURS_OFF:
					goToXYLCD(1, 0);
					printfLCD(getLabelString(L_TIMER_TIME_OFF_NO_HOURS_PATTERN), innerEndTimerHolder.minutes, innerEndTimerHolder.seconds);
					break;
					
				case SET_MINUTES_OFF:
					goToXYLCD(1, 0);
					printfLCD(getLabelString(L_TIMER_TIME_OFF_NO_MINUTES_PATTERN), innerEndTimerHolder.hours, innerEndTimerHolder.seconds);
					break;
					
				case SET_SECONDS_OFF:
					goToXYLCD(1, 0);
					printfLCD(getLabelString(L_TIMER_TIME_OFF_NO_SECONDS_PATTERN), innerEndTimerHolder.hours, innerEndTimerHolder.minutes);
					break;
			}
			
			if (needToEscapeToMenu) {
				break;
			}
			
			_delay_ms(DIGIT_BLINK_DELAY);
		}
		
		goBackToMenu();
}

static void dispalyTimeOn() {
	goToXYLCD(0, 0);
	printfLCD(getLabelString(L_TIMER_TIME_ON_PATTERN), innerStartTimerHolder.hours, innerStartTimerHolder.minutes, innerStartTimerHolder.seconds);
}

static void displayTimeOff() {
	goToXYLCD(1, 0);
	printfLCD(getLabelString(L_TIMER_TIME_OFF_PATTERN), innerEndTimerHolder.hours, innerEndTimerHolder.minutes, innerEndTimerHolder.seconds);
}

static void incrementHours(Timer *timer) {
	if (timer -> hours < (HOURS_IN_24H_FORMAT - 1)) {
		timer -> hours++;
	} else {
		timer -> hours = 0;
	}
}

static void incrementMinutes(Timer *timer) {
	if (timer -> minutes < (MINUTES_IN_HOUR - 1)) {
		timer -> minutes++;
	} else {
		timer -> minutes = 0;
	}
}

static void incrementSeconds(Timer *timer) {
	if (timer -> seconds < (SECONDS_IN_MINUTE - 1)) {
		timer -> seconds++;
	} else {
		timer -> seconds = 0;
	}
}

static void decrementHours(Timer *timer) {
	if (timer -> hours > 0) {
		timer -> hours--;
	} else {
		timer -> hours = HOURS_IN_24H_FORMAT - 1;
	}
}

static void decrementMinutes(Timer *timer) {
	if (timer -> minutes > 0) {
		timer -> minutes--;
	} else {
		timer -> minutes = MINUTES_IN_HOUR - 1;
	}
}

static void decrementSeconds(Timer *timer) {
	if (timer -> seconds > 0) {
		timer -> seconds--;
	} else {
		timer -> seconds = SECONDS_IN_MINUTE - 1;
	}
}

static _Bool isStartTimeBeforEndTime() {
	_Bool isStartTimeBeforEndTime = true;
	
	if ((innerStartTimerHolder.hours) > (innerEndTimerHolder.hours)) {
		isStartTimeBeforEndTime = false;
	}
	
	if ( (innerStartTimerHolder.hours) == (innerEndTimerHolder.hours) && 
		 (innerStartTimerHolder.minutes) > (innerEndTimerHolder.minutes) ) {
		isStartTimeBeforEndTime = false;
	}
	
	if ( (innerStartTimerHolder.hours) == (innerEndTimerHolder.hours)	  && 
		 (innerStartTimerHolder.minutes) == (innerEndTimerHolder.minutes) && 
		 (innerStartTimerHolder.seconds) > (innerEndTimerHolder.seconds) ) {
		isStartTimeBeforEndTime = false;
	}
	
	if (!isStartTimeBeforEndTime) {
		clearLCD();
		printLCDStringAtPosition(0, 0, getLabelString(L_START_TIME_AFTER_END_TIME));
		printLCDStringAtPosition(1, 0, getLabelString(L_START_TIME_AFTER_END_TIME2));
		_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
	}
	
	return isStartTimeBeforEndTime;
}

static void saveZoneTimerSettings(ZoneSettings *zoneSettings) {
	if (timerType == TIMER1_WATERING) {
		saveTimerValues(&zoneSettings -> waterTimer1.startTimer, &zoneSettings -> waterTimer1.endTimer);
	} else if (timerType == TIMER2_WATERING) {
		saveTimerValues(&zoneSettings -> waterTimer2.startTimer, &zoneSettings -> waterTimer2.endTimer);
	} else if (timerType == TIMER3_WATERING) {
		saveTimerValues(&zoneSettings -> waterTimer3.startTimer, &zoneSettings -> waterTimer3.endTimer);
	} else if (timerType == TIMER1_LIGHT) {
		saveTimerValues(&zoneSettings -> lightTimer1.startTimer, &zoneSettings -> lightTimer1.endTimer);
	} else if (timerType == TIMER2_LIGHT) {
		saveTimerValues(&zoneSettings -> lightTimer2.startTimer, &zoneSettings -> lightTimer2.endTimer);
	} else if (timerType == TIMER3_LIGHT) {
		saveTimerValues(&zoneSettings -> lightTimer3.startTimer, &zoneSettings -> lightTimer3.endTimer);
	}
	
	saveZoneSettingsToEEPROM(zoneSettings->zoneNumber, zoneSettings);
}

static void saveTimerValues(Timer *startTimer, Timer *endTimer) {
	startTimer -> hours = innerStartTimerHolder.hours;
	startTimer -> minutes = innerStartTimerHolder.minutes;
	startTimer -> seconds = innerStartTimerHolder.seconds;
	endTimer -> hours = innerEndTimerHolder.hours;
	endTimer -> minutes = innerEndTimerHolder.minutes;
	endTimer -> seconds = innerEndTimerHolder.seconds;
}

static void goBackToMenu() {
	needToEscapeToMenu = false;
	isMenuAlreadyViewed = false;
	disableInterruptTimer2();
	backToMenu();
}