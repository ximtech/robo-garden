#include "ZoneValidations.h"

static _Bool isTimerInProgress(Timer *startTimer, Timer *endTimer, bool *weekDays);
static void showErrorMessage();

_Bool isZoneTimerInProcess(ZoneSettings *zoneSettings, TimerType zoneTimerType) {
	if (zoneSettings -> isEnabled) {
		if ((zoneTimerType == TIMER1_WATERING || zoneTimerType == TIMER1_WATERING_WEEK_DAY) && 
			isTimerInProgress(&zoneSettings->waterTimer1.startTimer, &zoneSettings->waterTimer1.endTimer, zoneSettings->waterTimer1.weekDays)) {
			showErrorMessage();
			return true;
		
		} else if ((zoneTimerType == TIMER2_WATERING || zoneTimerType == TIMER2_WATERING_WEEK_DAY) && 
					isTimerInProgress(&zoneSettings->waterTimer2.startTimer, &zoneSettings->waterTimer2.endTimer, zoneSettings->waterTimer2.weekDays)) {
			showErrorMessage();
			return true;
		
		} else if ((zoneTimerType == TIMER3_WATERING || zoneTimerType == TIMER3_WATERING_WEEK_DAY) && 
					isTimerInProgress(&zoneSettings->waterTimer3.startTimer, &zoneSettings->waterTimer3.endTimer, zoneSettings->waterTimer3.weekDays)) {
			showErrorMessage();
			return true;
		
		} else if ((zoneTimerType == TIMER1_LIGHT || zoneTimerType == TIMER1_LIGHT_WEEK_DAY) && 
					isTimerInProgress(&zoneSettings->lightTimer1.startTimer, &zoneSettings->lightTimer1.endTimer, zoneSettings->lightTimer1.weekDays)) {
			showErrorMessage();
			return true;
		
		} else if ((zoneTimerType == TIMER2_LIGHT || zoneTimerType == TIMER2_LIGHT_WEEK_DAY) && 
					isTimerInProgress(&zoneSettings->lightTimer2.startTimer, &zoneSettings->lightTimer2.endTimer, zoneSettings->lightTimer2.weekDays)) {
			showErrorMessage();
			return true;
		
		} else if ((zoneTimerType == TIMER3_LIGHT || zoneTimerType == TIMER3_LIGHT_WEEK_DAY) && 
					isTimerInProgress(&zoneSettings->lightTimer3.startTimer, &zoneSettings->lightTimer3.endTimer, zoneSettings->lightTimer3.weekDays)) {
			showErrorMessage();
			return true;
		}
	}
	
	return false;
}

static _Bool isTimerInProgress(Timer *startTimer, Timer *endTimer, bool *weekDays) {
	if (!isZoneTimersEnabled(startTimer, endTimer) && !isWeekDayTimerEventOccured(weekDays)) {
		return false;
	}
	
	if (!isTimeEventOccured(startTimer, endTimer)) {
		return false;
	}
	return true;
}

static void showErrorMessage() {
	clearLCD();
	printLCDStringAtPosition(0, 1, getLabelString(L_ERR_TIMER));
	printLCDStringAtPosition(1, 1, getLabelString(L_ERR_IN_PROGRESS));
	_delay_ms(ERROR_MESSAGE_SHOW_TIMEOUT);
}
