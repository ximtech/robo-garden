#pragma once

#include "../../../MemoryModule/EEPROM.h"
#include "../../../TimerModule/TimerModule.h"

_Bool isZoneTimerInProcess(ZoneSettings *zoneSettings, TimerType zoneTimerType);