#include "DateTimeSettings.h"

typedef enum DateTimeSettingsMode {
	SET_HOURS	= 0,
	SET_MINUTES = 1,
	SET_DAY		= 2,
	SET_MONTH	= 3,
	SET_YEAR	= 4
} DateTimeSettingsMode;

struct DateTimeSettings {
	uint8_t hour;
	uint8_t minute;
	uint8_t day;
	uint8_t month;
	uint16_t year;
	DateTimeSettingsMode settingMode;
} dateTimeSettings = { 0, 0, FIRST_DAY_IN_MONTH, JANUARY, 0, SET_HOURS };
	
static volatile _Bool needToEscapeToMenu = false;
	

void processButtonsForDateTimeSettings() {
	
	debounce();
	
	if (button_down(BUTTON_UP_MASK)) {
		
		if (dateTimeSettings.settingMode == SET_HOURS) {
			if (dateTimeSettings.hour < (HOURS_IN_24H_FORMAT - 1)) {
				dateTimeSettings.hour++;
			} else {
				dateTimeSettings.hour = 0;
			}
			
		} else if (dateTimeSettings.settingMode == SET_MINUTES) {
			if (dateTimeSettings.minute < (MINUTES_IN_HOUR - 1)) {
				dateTimeSettings.minute++;
			} else {
				dateTimeSettings.minute = 0;
			}
			
		} else if (dateTimeSettings.settingMode == SET_DAY) {
			uint8_t dayInCurrentMonth = getDaysInMonth(dateTimeSettings.month, MIN_YEAR + dateTimeSettings.year);
			if (dateTimeSettings.day < dayInCurrentMonth) {
				dateTimeSettings.day++;
			} else {
				dateTimeSettings.day = FIRST_DAY_IN_MONTH;
			}
			
		} else if (dateTimeSettings.settingMode == SET_MONTH) {
			if (dateTimeSettings.month < (JANUARY + DECEMBER - 1)) {
				dateTimeSettings.month++;
			} else {
				dateTimeSettings.month = JANUARY;
			}
			
		} else if (dateTimeSettings.settingMode == SET_YEAR) {
			if (dateTimeSettings.year < MAX_YEAR) {
				dateTimeSettings.year++;
			} else {
				dateTimeSettings.year = MIN_YEAR;
			}
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_UP_PIN);
	}
	
	
	else if (button_down(BUTTON_DOWN_MASK)) {
		
		if (dateTimeSettings.settingMode == SET_HOURS) {
			if (dateTimeSettings.hour > 0) {
				dateTimeSettings.hour--;
			} else {
				dateTimeSettings.hour = HOURS_IN_24H_FORMAT - 1;
			}
			
		} else if (dateTimeSettings.settingMode == SET_MINUTES) {
			if (dateTimeSettings.minute > 0) {
				dateTimeSettings.minute--;
			} else {
				dateTimeSettings.minute = MINUTES_IN_HOUR - 1;
			}
			
		} else if (dateTimeSettings.settingMode == SET_DAY) {
			
			if (dateTimeSettings.day > FIRST_DAY_IN_MONTH) {
				dateTimeSettings.day--;
			} else {
				dateTimeSettings.day = getDaysInMonth(dateTimeSettings.month, dateTimeSettings.year);
			}
		
		} else if (dateTimeSettings.settingMode == SET_MONTH) {
			if (dateTimeSettings.month > JANUARY) {
			dateTimeSettings.month--;
			} else {
				dateTimeSettings.month = DECEMBER;
			}
		
		} else if (dateTimeSettings.settingMode == SET_YEAR) {
			if (dateTimeSettings.year > MIN_YEAR) {
				dateTimeSettings.year--;
			} else {
				dateTimeSettings.year = MAX_YEAR;
			}
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_DOWN_PIN);
	}
	
	
	else if (button_down(BUTTON_ENTER_MASK)) {
		
		if (dateTimeSettings.settingMode == SET_HOURS) {
			setRTChoursIn24hFormat(dateTimeSettings.hour);
		} else if (dateTimeSettings.settingMode == SET_MINUTES) {
			setRTCminutes(dateTimeSettings.minute);
		} else if (dateTimeSettings.settingMode == SET_DAY) {
			setRTCday(dateTimeSettings.day);
		} else if (dateTimeSettings.settingMode == SET_MONTH) {
			setRTCmonth(dateTimeSettings.month);
		} else if (dateTimeSettings.settingMode == SET_YEAR) {
			setRTCyear(dateTimeSettings.year);
		}
		
		setWeekDayByDate(dateTimeSettings.day, dateTimeSettings.month, MIN_YEAR + dateTimeSettings.year);
		
		if (dateTimeSettings.settingMode < (SET_HOURS + SET_YEAR)) {
			dateTimeSettings.settingMode++;
		} else {
			dateTimeSettings.settingMode = SET_HOURS;
		}
		
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_ENTER_PIN);
	}
	
	else if (button_down(BUTTON_BACK_MASK)) {
		needToEscapeToMenu = true;
		loop_until_bit_is_set(BUTTON_PIN, BUTTON_BACK_PIN);
	}
	
}

void setDateTime() {
	lockEnterButton();
	clearLCD();
	settingsItem = SET_DATE_TIME;
	
	LocalDateTime *localDateTime = getRTCdateTime();
	dateTimeSettings.hour = localDateTime -> hour;
	dateTimeSettings.minute = localDateTime -> minute;
	dateTimeSettings.day = localDateTime -> day;
	dateTimeSettings.month = localDateTime -> month;
	dateTimeSettings.year = localDateTime -> year;
	dateTimeSettings.settingMode = SET_HOURS;
	
	enableInterruptTimer2();
	
	while (!needToEscapeToMenu) {
		
		localDateTime = getRTCdateTime();
		
		switch(dateTimeSettings.settingMode) {
			case SET_HOURS:
				goToXYLCD(0, 0);
				dateTimeSettings.minute = localDateTime -> minute;	// for correct minute representation in next set, need update minutes when hours blinking
				printfLCD(getLabelString(L_SET_TIME_MAIN_PATTERN), dateTimeSettings.hour, localDateTime -> minute, localDateTime -> second);
				displayDate(localDateTime, getLabelString(L_SET_DATE_MAIN_PATTERN));		// do not move labels as variable pointer, works not as expected
				
				_delay_ms(DIGIT_BLINK_DELAY);
			
				goToXYLCD(0, 0);
				printfLCD(getLabelString(L_SET_TIME_NO_HOUR_PATTERN), localDateTime -> minute, localDateTime -> second);
				break;
			
			case SET_MINUTES:
				goToXYLCD(0, 0);
				printfLCD(getLabelString(L_SET_TIME_MAIN_PATTERN), localDateTime -> hour, dateTimeSettings.minute, localDateTime -> second);
				displayDate(localDateTime, getLabelString(L_SET_DATE_MAIN_PATTERN));
			
				_delay_ms(DIGIT_BLINK_DELAY);
				
				goToXYLCD(0, 0);
				printfLCD(getLabelString(L_SET_TIME_NO_MINUTES_PATTERN), localDateTime->hour, localDateTime -> second);
				break;
			
			case SET_DAY:
				displayTime(localDateTime, getLabelString(L_SET_TIME_MAIN_PATTERN));
				goToXYLCD(1, 0);
				dateTimeSettings.month = localDateTime -> month;
				printfLCD(getLabelString(L_SET_DATE_MAIN_PATTERN), dateTimeSettings.day, localDateTime -> month, localDateTime -> year, getWeekDayNameShort(localDateTime -> weekDay));
			
				_delay_ms(DIGIT_BLINK_DELAY);
				
				goToXYLCD(1, 0);
				printfLCD(getLabelString(L_SET_TIME_NO_DAYS_PATTERN), localDateTime -> month, localDateTime -> year, getWeekDayNameShort(localDateTime -> weekDay));
				break;
			
			case SET_MONTH:
				displayTime(localDateTime, getLabelString(L_SET_TIME_MAIN_PATTERN));
				goToXYLCD(1, 0);
				dateTimeSettings.year = localDateTime -> year;
				printfLCD(getLabelString(L_SET_DATE_MAIN_PATTERN), localDateTime -> day, dateTimeSettings.month, localDateTime -> year, getWeekDayNameShort(localDateTime -> weekDay));
			
				_delay_ms(DIGIT_BLINK_DELAY);
			
				goToXYLCD(1, 0);
				printfLCD(getLabelString(L_SET_TIME_NO_MONTH_PATTERN), localDateTime -> day, localDateTime -> year, getWeekDayNameShort(localDateTime -> weekDay));
				break;
			
			case SET_YEAR:
				displayTime(localDateTime, getLabelString(L_SET_TIME_MAIN_PATTERN));
				goToXYLCD(1, 0);
				printfLCD(getLabelString(L_SET_DATE_MAIN_PATTERN), localDateTime -> day, localDateTime -> month, dateTimeSettings.year, getWeekDayNameShort(localDateTime -> weekDay));
			
				_delay_ms(DIGIT_BLINK_DELAY);
				
				goToXYLCD(1, 0);
				printfLCD(getLabelString(L_SET_TIME_NO_YEAR_PATTERN), localDateTime -> day, localDateTime -> month, getWeekDayNameShort(localDateTime -> weekDay));
				break;
		}
		
		if (needToEscapeToMenu) {
			break;
		}
		
		_delay_ms(DIGIT_BLINK_DELAY);
	}
	
	needToEscapeToMenu = false;
	disableInterruptTimer2();
	backToMenu();
}

void displayTime(LocalDateTime *localDateTime, char *timePattern) {
	goToXYLCD(0, 0);
	printfLCD(timePattern, localDateTime -> hour, localDateTime -> minute, localDateTime -> second);
}

void displayDate(LocalDateTime *localDateTime, char *datePattern) {
	goToXYLCD(1, 0);
	printfLCD(datePattern, localDateTime -> day, localDateTime -> month, localDateTime -> year, getWeekDayNameShort(localDateTime -> weekDay));
}