#pragma once

#include "../CommonSettings.h"

void setDateTime();
void processButtonsForDateTimeSettings();
void displayTime(LocalDateTime *localDateTime, char *timePattern);
void displayDate(LocalDateTime *localDateTime, char *datePattern);