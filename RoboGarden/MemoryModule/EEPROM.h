#pragma once

#include "../SettingsModule/ZoneSettings/ZoneCommonSettings.h"
#include <avr/eeprom.h>


void saveZoneSettingsToEEPROM(ZoneNumber zoneNumber, ZoneSettings *zoneSettings);
void loadZoneSettingsFromEEPROM(ZoneNumber zoneNumber, ZoneSettings *zoneSettings);