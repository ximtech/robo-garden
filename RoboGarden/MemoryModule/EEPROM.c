#include "EEPROM.h"
#include <stdbool.h>


static ZoneSettings EEMEM zone1SettingsInEEPROM = {
	.zoneNumber = ZONE_1, .isEnabled = false,
	
	.soilData = { .sensorCount = 0, .humidityPercentage = 10 },
	
	.temperatureMax = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	.temperatureMin = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
		
	.waterTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }		// set all week days to true
	},
	
	.waterTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.waterTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	}
};
					
static ZoneSettings EEMEM zone2SettingsInEEPROM = {
	.zoneNumber = ZONE_2, .isEnabled = false,
	
	.soilData = { .sensorCount = 0, .humidityPercentage = 10 },
	
	.temperatureMax = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	.temperatureMin = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	
	.waterTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }		// set all week days to true
	},
	
	.waterTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.waterTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	}
};

static ZoneSettings EEMEM zone3SettingsInEEPROM = {
	.zoneNumber = ZONE_3, .isEnabled = false,
	
	.soilData = { .sensorCount = 0, .humidityPercentage = 10 },
	
	.temperatureMax = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	.temperatureMin = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	
	.waterTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }		// set all week days to true
	},
	
	.waterTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.waterTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	}
};

static ZoneSettings EEMEM zone4SettingsInEEPROM = {
	.zoneNumber = ZONE_4, .isEnabled = false,
	
	.soilData = { .sensorCount = 0, .humidityPercentage = 10 },
	
	.temperatureMax = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	.temperatureMin = { .integralT = 0, .decimalT = 0, .integralHyst = 0, .decimalHyst = 0 },
	
	.waterTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }		// set all week days to true
	},
	
	.waterTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.waterTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer1 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer2 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	},
	
	.lightTimer3 = {
		.startTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.endTimer = { .seconds = 0, .minutes = 0, .hours = 0 },
		.weekDays = { [0 ... 6] = true }
	}
};

void saveZoneSettingsToEEPROM(ZoneNumber zoneNumber, ZoneSettings *zoneSettings) {
	switch(zoneNumber) {
		case ZONE_1:
			eeprom_busy_wait();
			eeprom_update_block(zoneSettings, &zone1SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_2:
			eeprom_busy_wait();
			eeprom_update_block(zoneSettings, &zone2SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_3:
			eeprom_busy_wait();
			eeprom_update_block(zoneSettings, &zone3SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_4:
			eeprom_busy_wait();
			eeprom_update_block(zoneSettings, &zone4SettingsInEEPROM, sizeof(ZoneSettings));
			break;
	}
}

void loadZoneSettingsFromEEPROM(ZoneNumber zoneNumber, ZoneSettings *zoneSettings) {
	switch(zoneNumber) {
		case ZONE_1:
			eeprom_busy_wait();
			eeprom_read_block(zoneSettings, &zone1SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_2:
			eeprom_busy_wait();
			eeprom_read_block(zoneSettings, &zone2SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_3:
			eeprom_busy_wait();
			eeprom_read_block(zoneSettings, &zone3SettingsInEEPROM, sizeof(ZoneSettings));
			break;
		case ZONE_4:
			eeprom_busy_wait();
			eeprom_read_block(zoneSettings, &zone4SettingsInEEPROM, sizeof(ZoneSettings));
			break;
	}
}