#pragma once

#define F_CPU 8000000UL

#define PROJECT_NAME "Robo Garden"

#define PROJECT_VERSION_MAJOR 1
#define PROJECT_VERSION_MINOR 0